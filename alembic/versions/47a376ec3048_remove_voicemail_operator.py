"""Remove voicemail operator

Revision ID: 47a376ec3048
Revises: 2d9ffeaee224

"""

# revision identifiers, used by Alembic.
revision = '47a376ec3048'
down_revision = '2d9ffeaee224'

from alembic import op
from sqlalchemy import sql

staticvoicemail = sql.table('staticvoicemail',
                            sql.column('category'),
                            sql.column('commented'),
                            sql.column('var_name'),
                            sql.column('var_val'),
                            sql.column('filename'))


def upgrade():
    _remove_vm_operator()


def downgrade():
    _insert_vm_operator()


def _remove_vm_operator():
    op.execute(staticvoicemail
        .delete()
        .where(sql.and_(
        staticvoicemail.c.var_name == 'operator',
        staticvoicemail.c.category == 'general',
        staticvoicemail.c.filename == 'voicemail.conf'
    )))


def _insert_vm_operator():
    op.execute(staticvoicemail
               .insert()
               .values(var_name='operator',
                       var_val='no',
                       commented=0,
                       category='general',
                       filename='voicemail.conf'))

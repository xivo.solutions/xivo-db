"""add-alias-column-in-meetingroom-table

Revision ID: c999c3ae3073
Revises: 9bbd6bf151bc

"""

# revision identifiers, used by Alembic.
revision = 'c999c3ae3073'
down_revision = '9bbd6bf151bc'

from alembic import op 
import sqlalchemy as sa
from xivo_db import helper
import random
import string


# List modified/added tables
NEW_TABLES=[]
UPDATED_TABLES=['meetingroom']

meetingroom_table = sa.sql.table('meetingroom',
                                  sa.sql.column('id'),
                                  sa.sql.column('alias'))

# Function which updates schema (create/update tables)
def update_schema():
    # Create/Update table
    # Add types ...
    # Add foreing keys
    op.add_column('meetingroom',
                   sa.Column('alias', sa.String(25), nullable=True , unique=True))              

def all_meetingroom_where_alias_is_null():
    query = (
        sa.sql.select (
            [meetingroom_table.c.id ])
            .where(meetingroom_table.c.alias == None)
    )
    return op.get_bind().execute(query).fetchall() 

def generate_alias():
    source = string.ascii_lowercase + string.digits
    alias = ''.join((random.choice(source) for i in range(4))) + '-' + ''.join((random.choice(source) for i in range(4)))

    return alias

def generated_alias_exists(generated_alias):    
    query = (
        sa.sql.select(
            [meetingroom_table.c.id])
            .where(meetingroom_table.c.alias == generated_alias)
    )

    res = op.get_bind().execute(query).scalar()
    if res:
        return True
    else:
        return False

def update_mr_with_generated_alias(generated_alias, mr_id):
    update_mr = (meetingroom_table
                    .update()
                    .where(meetingroom_table.c.id == mr_id)
                    .values(alias = generated_alias))
    op.execute(update_mr)


def update_data(): 
    if helper.is_mds():
        pass
    else:     
        for row in all_meetingroom_where_alias_is_null():
            generated_alias = generate_alias()
            while generated_alias_exists(generated_alias):
                generated_alias = generate_alias()
            update_mr_with_generated_alias(generated_alias, row.id)
              
def upgrade():
    # Note: If you create a table using sqlalchemy command:
    #  - command will be run as asterisk,
    #  - therefore the owner's object will be asterisk
    #  - and therefore postgresql will give stats user the SELECT privilege

    if helper.is_mds():
        # Add here the actions applicable to MDS.
        # Don't forget that data are replicated from Main to MDS.

        # To update schema on MDS (see also the README.md) use this:
        with helper.schema_update_on_mds(op, NEW_TABLES, UPDATED_TABLES):
            update_schema()
        # Do not update **data** on MDS
    else:
        # Add here the actions applicable to XiVO Main
        # Don't forget that data are replicated from Main to MDS.

        # To update schema on Main (see also the README.md) use this:
        with helper.schema_update_on_main(op, NEW_TABLES):
            update_schema()
        # Update data on Main (and on main only, at least for all replicated tables)
        update_data()
        

def downgrade():
    pass

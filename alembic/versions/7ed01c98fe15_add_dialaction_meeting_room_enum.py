"""add_dialaction_meeting_room_enum

Revision ID: 7ed01c98fe15
Revises: 1e270909a41e

"""

# revision identifiers, used by Alembic.
revision = '7ed01c98fe15'
down_revision = '1e270909a41e'

from alembic import op

from xivo_db import helper

# List modified/added tables
NEW_TABLES = []
UPDATED_TABLES = []


# Function which updates schema (create/update tables)
def update_schema(op):
    connection = None
    if not op.get_context().as_sql:
        connection = op.get_bind()
        connection.execution_options(isolation_level='AUTOCOMMIT')

    op.execute("ALTER TYPE dialaction_action ADD VALUE 'meetingroom' AFTER 'meetme'")


def upgrade():
    if helper.is_mds():
        with helper.schema_update_on_mds(op, NEW_TABLES, UPDATED_TABLES):
            update_schema(op)
    else:
        with helper.schema_update_on_main(op, NEW_TABLES):
            update_schema(op)


def downgrade():
    pass

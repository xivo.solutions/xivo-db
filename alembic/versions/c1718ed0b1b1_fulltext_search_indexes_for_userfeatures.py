"""fulltext search indexes for userfeatures

Revision ID: c1718ed0b1b1
Revises: 449a10e23630

"""

# revision identifiers, used by Alembic.
revision = 'c1718ed0b1b1'
down_revision = '449a10e23630'

from alembic import op

from xivo_db import helper, postgres


def add_unaccent_extensions():
    new_extensions = ['unaccent']
    conn = postgres.create_db_connection('asterisk')
    postgres.create_extensions(conn, new_extensions)


# Function which updates schema (create/update tables)
def update_schema(op):
    op.execute("""
        CREATE INDEX userfeatures__idx__fullname__gin ON userfeatures USING GIN (to_tsvector('simple', firstname || ' ' || lastname));
        CREATE INDEX userfeatures__idx__description__gin ON userfeatures USING GIN (to_tsvector('simple', description));
        CREATE INDEX userfeatures__idx__userfield__gin ON userfeatures USING GIN (to_tsvector('simple', userfield));
        CREATE INDEX userfeatures__idx__email__gin ON userfeatures USING GIN (to_tsvector('simple', email));
    """)


def update_data(op):
    pass


def upgrade():
    if helper.is_mds():
        pass
    else:
        update_schema(op)
        add_unaccent_extensions()


def downgrade():
    pass

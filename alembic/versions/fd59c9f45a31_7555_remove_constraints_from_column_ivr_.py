"""7555 remove constraints from column ivr.ivredit_node.name

Revision ID: fd59c9f45a31
Revises: 393dcd9c9ce1

"""

# revision identifiers, used by Alembic.
revision = 'fd59c9f45a31'
down_revision = '393dcd9c9ce1'

from alembic import op
import sqlalchemy as sa
from xivo_db import helper


# List modified/added tables
NEW_TABLES=[]
UPDATED_TABLES=[]

# Function which updates schema (create/update tables)
def update_schema(op):
    # Create/Update table
    # Add types ...
    # Add foreing keys

    table_full = 'ivr.ivredit_node'
    op.execute("ALTER TABLE IF EXISTS " + table_full + " DROP CONSTRAINT IF EXISTS ivredit_node_id_flowchart_name_key")
    # check constraint on "name" column can have 2 different names
    op.execute("ALTER TABLE IF EXISTS " + table_full + " DROP CONSTRAINT IF EXISTS ivredit_node_name_check")
    op.execute("ALTER TABLE IF EXISTS " + table_full + " DROP CONSTRAINT IF EXISTS ivredit_node_name_empty_string")

def update_data(op):
    pass

def upgrade():
    # Note: If you create a table using sqlalchemy command:
    #  - command will be run as asterisk,
    #  - therefore the owner's object will be asterisk
    #  - and therefore postgresql will give stats user the SELECT privilege

    if helper.is_mds():
        # CAUTION: do not take this as an example
        # this is completely custom because ivr table are not in the replication
        update_schema(op)
    else:
        # Add here the actions applicable to XiVO Main
        # Don't forget that data are replicated from Main to MDS.

        # To update schema on Main (see also the README.md) use this:
        with helper.schema_update_on_main(op, NEW_TABLES):
            update_schema(op)
        # Update data on Main (and on main only, at least for all replicated tables)
        update_data(op)


def downgrade():
    pass

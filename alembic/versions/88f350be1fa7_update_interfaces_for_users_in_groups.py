"""update interfaces for users in groups

Revision ID: 88f350be1fa7
Revises: 98b9ca9958e0

"""

# revision identifiers, used by Alembic.
revision = '88f350be1fa7'
down_revision = '98b9ca9958e0'

from alembic import op
import sqlalchemy as sa
from xivo_db import helper
from sqlalchemy import sql, and_, or_


# List modified/added tables
NEW_TABLES=[]
UPDATED_TABLES=[]

queuemember_table = sql.table('queuemember',
                        sql.column('interface'),
                        sql.column('userid'),
                        sql.column('usertype'),
                        sql.column('category'))

# Function which updates schema (create/update tables)
def update_schema(op):
    # Create/Update table
    # Add types ...
    # Add foreing keys
    pass

def update_data(op):
    if helper.is_mds():
        pass
    else:
        # Update data
        op.execute(queuemember_table
            .update()
            .where(and_(queuemember_table.c.usertype == 'user',
              or_(queuemember_table.c.category == 'group', 
                queuemember_table.c.category == 'queue')
            ))
            .values(interface = 'Local/id-' + queuemember_table.c.userid + '@usercallback'))
        pass


def upgrade():
    # Note: If you create a table using sqlalchemy command:
    #  - command will be run as asterisk,
    #  - therefore the owner's object will be asterisk
    #  - and therefore postgresql will give stats user the SELECT privilege

    if helper.is_mds():
        # Add here the actions applicable to MDS.
        # Don't forget that data are replicated from Main to MDS.

        # To update schema on MDS (see also the README.md) use this:
        with helper.schema_update_on_mds(op, NEW_TABLES, UPDATED_TABLES):
            update_schema(op)
        # Do not update **data** on MDS
        pass
    else:
        # Add here the actions applicable to XiVO Main
        # Don't forget that data are replicated from Main to MDS.

        # To update schema on Main (see also the README.md) use this:
        with helper.schema_update_on_main(op, NEW_TABLES):
            update_schema(op)
        # Update data on Main (and on main only, at least for all replicated tables)
        update_data(op)


def downgrade():
    pass

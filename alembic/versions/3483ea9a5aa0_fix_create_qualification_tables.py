"""fix create qualification tables

Revision ID: 3483ea9a5aa0
Revises: 4625c5a98af1

"""

# revision identifiers, used by Alembic.
revision = '3483ea9a5aa0'
down_revision = '4625c5a98af1'

import sqlalchemy as sa
from alembic import op


def check_table_exists(table):
    sql = "SELECT EXISTS (SELECT 1 " \
          "FROM information_schema.tables " \
          "WHERE table_schema = 'public' " \
          "AND table_name='" + table + "')"

    result_proxy = op.get_bind().execute(sql)
    exists = result_proxy.fetchone()[0]

    return exists


def upgrade():
    if not check_table_exists('qualifications'):
        op.create_table(
            'qualifications',
            sa.Column('id', sa.Integer),
            sa.Column('name', sa.String(128), nullable=False),
            sa.Column('active', sa.Integer, nullable=False, server_default='1'),
            sa.PrimaryKeyConstraint('id'),
        )
        op.execute('GRANT ALL PRIVILEGES ON TABLE qualifications TO configmgt')
        op.execute('GRANT ALL PRIVILEGES ON TABLE qualifications TO stats')
        op.execute('GRANT ALL ON qualifications_id_seq to configmgt')
        op.execute('GRANT ALL ON qualifications_id_seq to stats')

    if not check_table_exists('subqualifications'):
        op.create_table(
            'subqualifications',
            sa.Column('id', sa.Integer),
            sa.Column('name', sa.String(128), nullable=False),
            sa.Column('qualification_id', sa.Integer, nullable=False),
            sa.Column('active', sa.Integer, nullable=False, server_default='1'),
            sa.PrimaryKeyConstraint('id'),
            sa.ForeignKeyConstraint(['qualification_id'], ['qualifications.id'])
        )
        op.execute('GRANT ALL PRIVILEGES ON TABLE subqualifications TO configmgt')
        op.execute('GRANT ALL PRIVILEGES ON TABLE subqualifications TO stats')
        op.execute('GRANT ALL ON subqualifications_id_seq to configmgt')
        op.execute('GRANT ALL ON subqualifications_id_seq to stats')

    if not check_table_exists('queue_qualification'):
        op.create_table(
            'queue_qualification',
            sa.Column('id', sa.Integer),
            sa.Column('queue_id', sa.Integer, nullable=False),
            sa.Column('qualification_id', sa.Integer, nullable=False),
            sa.PrimaryKeyConstraint('id')
        )
        op.execute('GRANT ALL PRIVILEGES ON TABLE queue_qualification TO configmgt')
        op.execute('GRANT ALL PRIVILEGES ON TABLE queue_qualification TO stats')
        op.execute('GRANT ALL ON queue_qualification_id_seq to configmgt')
        op.execute('GRANT ALL ON queue_qualification_id_seq to stats')

    if not check_table_exists('qualification_answers'):
        op.create_table(
            'qualification_answers',
            sa.Column('id', sa.Integer),
            sa.Column('sub_qualification_id', sa.Integer),
            sa.Column('time', sa.DateTime, server_default=sa.text("(current_timestamp at time zone 'utc')")),
            sa.Column('callid', sa.String(128), nullable=False),
            sa.Column('agent', sa.Integer),
            sa.Column('queue', sa.Integer),
            sa.Column('first_name', sa.String(128), nullable=True),
            sa.Column('last_name', sa.String(128), nullable=True),
            sa.Column('comment', sa.String(255), nullable=True),
            sa.Column('custom_data', sa.Text, nullable=True),
            sa.PrimaryKeyConstraint('id')
        )
        op.execute('GRANT ALL PRIVILEGES ON TABLE qualification_answers TO configmgt')
        op.execute('GRANT ALL PRIVILEGES ON TABLE qualification_answers TO stats')
        op.execute('GRANT ALL ON qualification_answers_id_seq to configmgt')
        op.execute('GRANT ALL ON qualification_answers_id_seq to stats')
    else:
        op.alter_column('qualification_answers', 'time',
                        server_default=sa.text("(current_timestamp at time zone 'utc')"))


def downgrade():
    pass

"""add_groups_mediaserverid

Revision ID: b57eb2e42524
Revises: 8a6ad713c519

"""

# revision identifiers, used by Alembic.
revision = 'b57eb2e42524'
down_revision = '8a6ad713c519'

import sqlalchemy as sa
from alembic import op


def upgrade():
    op.add_column('groupfeatures',
                  sa.Column('mediaserverid', sa.Integer, nullable=False, server_default='0'))


def downgrade():
    op.drop_column('groupfeatures', 'mediaserverid')

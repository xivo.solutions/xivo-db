"""Add meetingroom table

Revision ID: 1e270909a41e
Revises: ea47634ce5c3

"""

# revision identifiers, used by Alembic.
revision = '1e270909a41e'
down_revision = 'ea47634ce5c3'

import sqlalchemy as sa
from alembic import op

from xivo_db import helper

# List modified/added tables
NEW_TABLES = ['meetingroom']
UPDATED_TABLES = []


# Function which updates schema (create/update tables)
def update_schema(op):
    op.create_table(
        'meetingroom',
        sa.Column('id', sa.Integer, nullable=False, autoincrement=True),
        sa.Column('name', sa.String(128), nullable=False),
        sa.Column('display_name', sa.String(128), nullable=False),
        sa.Column('number', sa.Integer, nullable=True),
        sa.Column('user_pin', sa.Integer, nullable=True),
        sa.PrimaryKeyConstraint('id'),
        sa.UniqueConstraint('name'),
        sa.UniqueConstraint('number')
    )


def upgrade():
    if helper.is_mds():
        with helper.schema_update_on_mds(op, NEW_TABLES, UPDATED_TABLES):
            update_schema(op)
    else:
        with helper.schema_update_on_main(op, NEW_TABLES):
            update_schema(op)


def downgrade():
    pass

"""fix_clean_outcall_from_contextmember_table

Revision ID: 420ed0e31a38
Revises: a62761e1efdb

"""

# revision identifiers, used by Alembic.
revision = '420ed0e31a38'
down_revision = 'a62761e1efdb'

import sqlalchemy as sa
from alembic import op

from xivo_db import helper

contextmember_table = sa.sql.table('contextmember',
                                   sa.sql.column('type')
                                   )


def upgrade():
    if helper.is_mds():
        # Delete nothing on MDS as data are replicated
        pass
    else:
        delete_outcall_contextmember = (contextmember_table
                                        .delete()
                                        .where(contextmember_table.c.type == 'outcall'))
        op.get_bind().execute(delete_outcall_contextmember)


def downgrade():
    pass

"""add contacts tables

Revision ID: f117d0b7524a
Revises: 7ee3e3344905

"""
# revision identifiers, used by Alembic.


import sqlalchemy as sa
from alembic import op

from xivo_db import helper

# revision identifiers, used by Alembic.
revision = 'f117d0b7524a'
down_revision = '7ee3e3344905'


def upgrade():
    if helper.is_mds():
        pass
    else:

        op.create_table(
            'contact',
            sa.Column('id', sa.Integer, nullable=False),
            sa.Column('firstname', sa.String(128), nullable=False, default=''),
            sa.Column('lastname', sa.String(128), nullable=False, default=''),
            sa.Column('address', sa.String(128), nullable=True),
            sa.Column('title', sa.String(128), nullable=True),
            sa.Column('company', sa.String(128), nullable=True),
            sa.PrimaryKeyConstraint('id'),
            sa.UniqueConstraint('id'),
        )

        op.create_table(
            'contact_number',
            sa.Column('id', sa.Integer, nullable=False),
            sa.Column('contact_id', sa.Integer, nullable=False),
            sa.Column('number', sa.String(128), nullable=False, ),
            sa.Column('number_type', sa.String(128), nullable=False, ),
            sa.PrimaryKeyConstraint('id'),

        )

        op.create_foreign_key(constraint_name='contact_number_contact_id_fkey', source_table='contact_number',
                              referent_table='contact', local_cols=['contact_id'], remote_cols=['id'],
                              ondelete='CASCADE', )

        op.create_table(
            'contact_source',
            sa.Column('id', sa.Integer, nullable=False),
            sa.Column('id_in_source', sa.Integer, nullable=False),
            sa.Column('source', sa.String(128), nullable=False, ),
            sa.Column('contact_id', sa.Integer, nullable=False),

            sa.PrimaryKeyConstraint('id'),

        )

        op.create_foreign_key(constraint_name='contact_source_contact_id_fkey', source_table='contact_source',
                              referent_table='contact', local_cols=['contact_id'], remote_cols=['id'],
                              ondelete='CASCADE', )

        op.create_table(
            'contact_email',
            sa.Column('id', sa.Integer, nullable=False),
            sa.Column('contact_id', sa.Integer, nullable=False),
            sa.Column('email', sa.String(128), nullable=False, ),
            sa.PrimaryKeyConstraint('id'),

        )

        op.create_foreign_key(constraint_name='contact_email_contact_id_fkey', source_table='contact_email',
                              referent_table='contact', local_cols=['contact_id'], remote_cols=['id'],
                              ondelete='CASCADE', )

        op.create_table(
            'contact_label',
            sa.Column('id', sa.Integer, nullable=False),
            sa.Column('label_name', sa.String, nullable=False),
            sa.Column('description', sa.String(128), nullable=True),
            sa.PrimaryKeyConstraint('id'),

        )

        op.create_table(
            'contact_label_contact',
            sa.Column('id', sa.Integer, nullable=False),
            sa.Column('contact_id', sa.Integer, nullable=False),
            sa.Column('contact_label_id', sa.Integer, nullable=False),
            sa.PrimaryKeyConstraint('id'),
        )

        op.create_foreign_key(constraint_name='contact_label_contact_contact_label_id_fkey',
                              source_table='contact_label_contact',
                              referent_table='contact_label', local_cols=['contact_label_id'], remote_cols=['id'],
                              ondelete='CASCADE', )
        op.create_foreign_key(constraint_name='contact_label_contact_contact_id_fkey',
                              source_table='contact_label_contact',
                              referent_table='contact', local_cols=['contact_id'], remote_cols=['id'],
                              ondelete='CASCADE', )


def downgrade():
    pass

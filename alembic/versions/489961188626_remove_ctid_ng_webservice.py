"""remove-ctid-ng-webservice

Revision ID: 489961188626
Revises: 4008f033a789

"""

# revision identifiers, used by Alembic.
revision = '489961188626'
down_revision = '4008f033a789'

from alembic import op
from sqlalchemy import sql
from sqlalchemy.sql import func

webservice = sql.table('accesswebservice',
                       sql.column('name'),
                       sql.column('login'),
                       sql.column('passwd'),
                       sql.column('acl'),
                       sql.column('description'))


def upgrade():
    _delete_web_service('xivo-ctid-ng')


def _delete_web_service(name):
    op.execute(webservice
               .delete()
               .where(webservice.c.name == name))


def downgrade():
    _insert_web_service('xivo-ctid-ng',
                        'xivo-ctid-ng',
                        '{confd.#, amid.action.Redirect.create, amid.action.Setvar.create}',
                        'Automatically created during downgrade')


def _insert_web_service(name, login, acl, description):
    op.execute(webservice
               .insert()
               .values(name=name,
                       login=login,
                       passwd=func.substring(func.gen_salt('bf', 4), 8),
                       acl=acl,
                       description=description))

"""fix_empty_string_within_dialpattern

Revision ID: a62761e1efdb
Revises: 4a22caaf0562

"""

# revision identifiers, used by Alembic.
revision = 'a62761e1efdb'
down_revision = '4a22caaf0562'

import sqlalchemy as sa
from alembic import op

from xivo_db import helper

routepattern = sa.sql.table('routepattern',
                            sa.sql.column('id'),
                            sa.sql.column('pattern'),
                            sa.sql.column('regexp'),
                            sa.sql.column('target'),
                            sa.sql.column('callerid'),
                            sa.sql.column('routeid'))


def upgrade():
    if helper.is_mds():
        # Update nothing on MDS as data are replicated
        pass
    else:
        op.execute(
            routepattern
                .update()
                .values(regexp=None)
                .where(routepattern.c.regexp == ''))

        op.execute(
            routepattern
                .update()
                .values(target=None)
                .where(routepattern.c.target == ''))

        op.execute(
            routepattern
                .update()
                .values(callerid=None)
                .where(routepattern.c.callerid == ''))


def downgrade():
    pass

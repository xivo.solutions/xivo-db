"""Empty stunaddr value

Revision ID: 184a72a2c4c7
Revises: be9fc1c82a2e

"""

# revision identifiers, used by Alembic.
revision = '184a72a2c4c7'
down_revision = 'be9fc1c82a2e'

from alembic import op

from xivo_db import helper

# List modified/added tables
NEW_TABLES = []
UPDATED_TABLES = []


# Function which updates schema (create/update tables)
def update_schema(op):
    # Create/Update table
    # Add types ...
    # Add foreing keys
    pass


def update_data(op):
    if helper.is_mds():
        pass
    else:
        # Update data
        op.execute("UPDATE staticsip SET var_val = null WHERE var_name='stunaddr'")


def upgrade():
    # Note: If you create a table using sqlalchemy command:
    #  - command will be run as asterisk,
    #  - therefore the owner's object will be asterisk
    #  - and therefore postgresql will give stats user the SELECT privilege

    if helper.is_mds():
        # Do not update **data** on MDS
        pass
    else:
        # Update data on Main (and on main only, at least for all replicated tables)
        update_data(op)


def downgrade():
    pass

"""Add type and user id to meeting room table

Revision ID: 533bdd6f24ed
Revises: f117d0b7524a

"""

from alembic import op
# revision identifiers, used by Alembic.
from sqlalchemy import String, Column, Integer

from xivo_db import helper

revision = '533bdd6f24ed'
down_revision = 'f117d0b7524a'

# List modified/added tables
NEW_TABLES = []
UPDATED_TABLES = ['meetingroom', 'userfeatures']


# Function which updates schema (create/update tables)
def update_schema(op):
    op.add_column('meetingroom',
                  Column('room_type', String(40), nullable=False, server_default='static'))
    op.add_column('meetingroom',
                  Column('user_id', Integer, nullable=True))
    op.create_foreign_key(
        'meetingroom_user_id_fkey',
        'meetingroom',
        'userfeatures',
        ['user_id'],
        ['id'],
        ondelete='CASCADE',
    )


def upgrade():
    if helper.is_mds():
        with helper.schema_update_on_mds(op, NEW_TABLES, UPDATED_TABLES):
            update_schema(op)
        pass
    else:
        with helper.schema_update_on_main(op, NEW_TABLES):
            update_schema(op)


def downgrade():
    pass

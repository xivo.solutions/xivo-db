"""Add_mobile_push_token_column_to_userfeatures

Revision ID: e736e9c74b6a
Revises: 52a21b7615ec

"""

# revision identifiers, used by Alembic.
revision = 'e736e9c74b6a'
down_revision = '52a21b7615ec'

from alembic import op
from sqlalchemy.sql.expression import true
from sqlalchemy.sql.schema import Column
from sqlalchemy.sql.sqltypes import VARCHAR

from xivo_db import helper

# List modified/added tables
NEW_TABLES = []
UPDATED_TABLES = ['userfeatures']


# Function which updates schema (create/update tables)
def update_schema(op):
    # Create/Update table
    # Add types ...
    # Add foreing keys
    op.add_column('userfeatures',
                  Column('mobile_push_token', VARCHAR(255), nullable=true))


def update_data(op):
    if helper.is_mds():
        # Do not update **data** on MDS
        pass
    else:
        # Update data
        pass


def upgrade():
    if helper.is_mds():
        # To update schema on MDS (see also the README.md) use this:
        with helper.schema_update_on_mds(op, NEW_TABLES, UPDATED_TABLES):
            update_schema(op)
        # Do not update **data** on MDS
    else:
        # To update schema on Main (see also the README.md) use this:
        with helper.schema_update_on_main(op, NEW_TABLES):
            update_schema(op)
        # Update data on Main (and on main only, at least for all replicated tables)
        update_data(op)


def downgrade():
    pass

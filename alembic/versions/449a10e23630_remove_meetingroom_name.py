"""remove meetingroom name

Revision ID: 449a10e23630
Revises: 19c66350e9c5

"""

# revision identifiers, used by Alembic.
revision = '449a10e23630'
down_revision = '19c66350e9c5'

from alembic import op

from xivo_db import helper

# List modified/added tables
NEW_TABLES = []
UPDATED_TABLES = ['meetingroom']


# Function which updates schema (create/update tables)
def update_schema(op):
    # Create/Update table
    # Add types ...
    # Add foreing keys
    op.drop_column('meetingroom', 'name')


def update_data(op):
    if helper.is_mds():
        pass
    else:
        # Update data
        pass


def upgrade():
    # Note: If you create a table using sqlalchemy command:
    #  - command will be run as asterisk,
    #  - therefore the owner's object will be asterisk
    #  - and therefore postgresql will give stats user the SELECT privilege

    if helper.is_mds():
        # Add here the actions applicable to MDS.
        # Don't forget that data are replicated from Main to MDS.

        # To update schema on MDS (see also the README.md) use this:
        with helper.schema_update_on_mds(op, NEW_TABLES, UPDATED_TABLES):
            update_schema(op)
        # Do not update **data** on MDS
        pass
    else:
        # Add here the actions applicable to XiVO Main
        # Don't forget that data are replicated from Main to MDS.

        # To update schema on Main (see also the README.md) use this:
        with helper.schema_update_on_main(op, NEW_TABLES):
            update_schema(op)
        # Update data on Main (and on main only, at least for all replicated tables)
        update_data(op)


def downgrade():
    pass

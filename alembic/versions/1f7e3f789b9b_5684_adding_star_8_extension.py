"""5684 adding star 8 extension

Revision ID: 1f7e3f789b9b
Revises: c999c3ae3073

"""

# revision identifiers, used by Alembic.
revision = '1f7e3f789b9b'
down_revision = 'c999c3ae3073'

from alembic import op
import sqlalchemy as sa
from xivo_db import helper

UPDATED_TABLES=['extensions']

def upgrade():
    if helper.is_mds():
        pass
    else:
        op.execute("""
            INSERT INTO "extensions" (commented, context, exten, type, typeval) VALUES (0, 'xivo-features', '*8', 'extenfeatures', 'group-pickup');
        """)


def downgrade():
    op.execute("""
        DELETE FROM "extensions" WHERE typeval='group-pickup';
    """)
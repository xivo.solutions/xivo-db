"""Remove invalid queue members

Revision ID: 759e04ad2ef9
Revises: d48abcaf9f38

"""

# revision identifiers, used by Alembic.
revision = '759e04ad2ef9'
down_revision = 'd48abcaf9f38'

import sqlalchemy as sa
from alembic import op
from sqlalchemy.sql import functions

from xivo_db import helper

queuemember_table = sa.sql.table('queuemember',
                                 sa.sql.column('queue_name'),
                                 sa.sql.column('interface'),
                                 sa.sql.column('usertype'),
                                 sa.sql.column('userid'),
                                 sa.sql.column('channel'),
                                 sa.sql.column('category'))

user_line_table = sa.sql.table('user_line',
                               sa.sql.column('user_id'),
                               sa.sql.column('line_id'))

linefeatures_table = sa.sql.table('linefeatures',
                                  sa.sql.column('id'),
                                  sa.sql.column('name'),
                                  sa.sql.column('protocol'))

userfeatures_table = sa.sql.table('userfeatures',
                                  sa.sql.column('id'),
                                  sa.sql.column('firstname'),
                                  sa.sql.column('lastname'))

join_conditions = (queuemember_table
                   .outerjoin(user_line_table,
                              queuemember_table.c.userid == user_line_table.c.user_id)
                   .outerjoin(linefeatures_table,
                              user_line_table.c.line_id == linefeatures_table.c.id)
                   .outerjoin(userfeatures_table,
                              queuemember_table.c.userid == userfeatures_table.c.id))

where_removed_interfaces = sa.sql.and_(
    sa.sql.or_(
        sa.sql.and_(
            queuemember_table.c.channel == 'SIP',
            linefeatures_table.c.protocol == 'sip'
        ),
        sa.sql.and_(
            queuemember_table.c.channel == 'SCCP',
            linefeatures_table.c.protocol == 'sccp'
        )
    ),
    queuemember_table.c.interface != functions.concat(
        queuemember_table.c.channel,
        '/',
        linefeatures_table.c.name
    ),
)

where_members_without_line = sa.sql.and_(
    linefeatures_table.c.protocol == None,
    user_line_table.c.line_id == None
)

find_invalid_queuemembers_query = (
    sa.sql.select([
        queuemember_table.c.queue_name,
        queuemember_table.c.interface,
        queuemember_table.c.usertype,
        queuemember_table.c.userid,
        queuemember_table.c.channel,
        queuemember_table.c.category,
        user_line_table.c.line_id,
        linefeatures_table.c.name,
        userfeatures_table.c.firstname,
        userfeatures_table.c.lastname
    ],
        from_obj=[join_conditions])
        .where(
        sa.sql.and_(
            sa.sql.or_(
                where_removed_interfaces,
                where_members_without_line
            ),
            queuemember_table.c.usertype == 'user'
        )
    ))


def find_invalid_queuemembers_with_line():
    result_proxy = op.get_bind().execute(find_invalid_queuemembers_query)

    return result_proxy


def print_log(qm):
    fullname = u" ".join([_f for _f in [qm.firstname, qm.lastname] if _f])

    if qm.line_id is not None:
        print(u'[MIGRATE_QUEUEMEMBER] : Deleting member "%s" from %s "%s" (user "%s" has line "%s" id %s) ' %
              (qm.interface,
               qm.category,
               qm.queue_name,
               fullname,
               qm.name,
               qm.line_id
               ))
    else:
        print(u'[MIGRATE_QUEUEMEMBER] : Deleting member "%s" from %s "%s" (user "%s" has no line) ' %
              (qm.interface,
               qm.category,
               qm.queue_name,
               fullname
               ))


def delete_invalid_queuemember_with_line(qm):
    delete_invalid_queuemembers_query = (
        queuemember_table
            .delete()
            .where(
            sa.sql.and_(
                queuemember_table.c.queue_name == qm.queue_name,
                queuemember_table.c.interface == qm.interface,
                queuemember_table.c.usertype == 'user',
                queuemember_table.c.userid == qm.userid,
                queuemember_table.c.channel == qm.channel,
                queuemember_table.c.category == qm.category,
            )
        ))

    op.get_bind().execute(delete_invalid_queuemembers_query)


def upgrade():
    if helper.is_mds():
        pass
    else:
        queuemembers = find_invalid_queuemembers_with_line()
        if queuemembers.rowcount > 0:
            for qm in queuemembers:
                print_log(qm)
                delete_invalid_queuemember_with_line(qm)


def downgrade():
    pass

"""4331_turn_int_into_string_for_meetingroom_pin_and_number

Revision ID: 7ee3e3344905
Revises: 8f97c2fa9b1e

"""

# revision identifiers, used by Alembic.
revision = '7ee3e3344905'
down_revision = '8f97c2fa9b1e'

from alembic import op
from sqlalchemy.types import String

from xivo_db import helper

# List modified/added tables
NEW_TABLES = []
UPDATED_TABLES = ['meetingroom']


# Function which updates schema (create/update tables)
def update_schema(op):
    op.alter_column(table_name='meetingroom', column_name='number', type_=String(40))
    op.alter_column(table_name='meetingroom', column_name='user_pin', type_=String(12))
    pass


def upgrade():
    if helper.is_mds():
        with helper.schema_update_on_mds(op, NEW_TABLES, UPDATED_TABLES):
            update_schema(op)
        pass
    else:
        with helper.schema_update_on_main(op, NEW_TABLES):
            update_schema(op)


def downgrade():
    pass

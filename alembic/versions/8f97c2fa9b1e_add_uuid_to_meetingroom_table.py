"""add_uuid-to-meetingroom-table

Revision ID: 8f97c2fa9b1e
Revises: 7ed01c98fe15

"""

# revision identifiers, used by Alembic.
revision = '8f97c2fa9b1e'
down_revision = '7ed01c98fe15'

from alembic import op
from sqlalchemy import text
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.schema import (Column)

from xivo_db import helper

# List modified/added tables
NEW_TABLES = []
UPDATED_TABLES = ['meetingroom']


# Function which updates schema (create/update tables)
def update_schema(op):
    op.add_column('meetingroom',
                  Column('uuid', UUID(as_uuid=True), nullable=False, server_default=text('uuid_generate_v4()')))
    pass


def upgrade():
    if helper.is_mds():
        with helper.schema_update_on_mds(op, NEW_TABLES, UPDATED_TABLES):
            update_schema(op)
        pass
    else:
        with helper.schema_update_on_main(op, NEW_TABLES):
            update_schema(op)


def downgrade():
    pass

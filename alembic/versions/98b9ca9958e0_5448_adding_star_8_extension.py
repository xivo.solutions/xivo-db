"""5684 adding star 8 extension

Revision ID: 98b9ca9958e0
Revises: cbf55beb9ff6

"""

# revision identifiers, used by Alembic.
from sqlalchemy import sql
from sqlalchemy.dialects.postgresql import insert
from xivo_db import helper
import sqlalchemy as sa
from alembic import op
revision = '98b9ca9958e0'
down_revision = 'cbf55beb9ff6'


UPDATED_TABLES = ['extensions']
extensions_table = sql.table('extensions',
                             sql.column('commented'),
                             sql.column('context'),
                             sql.column('type'),
                             sql.column('typeval'),
                             sql.column('exten'))


def upgrade():
    if helper.is_mds():
        pass
    else:
        op.execute(
          insert(extensions_table).values(commented=0, context='xivo-features', exten='*8', type='extenfeatures', typeval='group-pickup').on_conflict_do_nothing()
        )


def downgrade():
    pass

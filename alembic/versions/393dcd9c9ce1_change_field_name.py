"""change field name

Revision ID: 393dcd9c9ce1
Revises: 638f77d2afba

"""

# revision identifiers, used by Alembic.
revision = '393dcd9c9ce1'
down_revision = '638f77d2afba'

from alembic import op
import sqlalchemy as sa
from xivo_db import helper

cti_displays = sa.sql.table('ctidisplays',
                            sa.sql.column('description'),
                            sa.sql.column('name'),
                            sa.sql.column('data'),
                            sa.sql.column('deletable'))

value = ('{' +
        '"10":  ["Nom","name","","header_1_info"],'+
        '"20":  ["Subtitle1","subtitle1","","header_2_info"],'+
        '"30":  ["Subtitle2","subtitle2","","header_3_info"],'+
        '"40":  ["Téléphone interne","number","","contact_1_callable"],'+
        '"50":  ["Téléphone pro","callable","","contact_2_callable"],'+
        '"60":  ["Téléphone mobile","callable","","contact_3_callable"],'+
        '"70":  ["Téléphone autre","callable","","contact_4_callable"],'+
        '"80":  ["E-mail","email","","contact_5_email"],'+
        '"90":  ["Télécopie","callable","","contact_6_callable"],'+
        '"100": ["Fonction","info","","general_1_info"],'+
        '"110": ["Service","info","","general_2_info"],'+
        '"120": ["Manager","info","","general_3_info"],'+
        '"130": ["Société","info","","general_4_info"],'+
        '"140": ["Site web","info","","general_5_info"],'+
        '"150": ["Bureau","info","","location_1_info"],'+
        '"160": ["Adresse","info","","location_2_info"],'+
        '"170": ["","favorite","","favorite"],'+
        '"180": ["","personal","",""],'+
        '"190": ["Avatar","picture","","picture"]}')


def change_field_name():
    op.execute(cti_displays
            .delete()
            .where(cti_displays.c.name == 'Display'))
    op.execute(cti_displays
            .insert()
            .values(name='Display',
                    data=value,
                    description='Affichage par défaut',
                    deletable=0))

# List modified/added tables
NEW_TABLES=[]
UPDATED_TABLES=[]

# Function which updates schema (create/update tables)
def update_schema(op):
    # Create/Update table
    # Add types ...
    # Add foreing keys
    pass

def update_data(op):
    if helper.is_mds():
        pass
    else:
        # Update data
        change_field_name()


def upgrade():
    # Note: If you create a table using sqlalchemy command:
    #  - command will be run as asterisk,
    #  - therefore the owner's object will be asterisk
    #  - and therefore postgresql will give stats user the SELECT privilege

    if helper.is_mds():
        # Add here the actions applicable to MDS.
        # Don't forget that data are replicated from Main to MDS.

        # To update schema on MDS (see also the README.md) use this:
        with helper.schema_update_on_mds(op, NEW_TABLES, UPDATED_TABLES):
            update_schema(op)
        # Do not update **data** on MDS
        pass
    else:
        # Add here the actions applicable to XiVO Main
        # Don't forget that data are replicated from Main to MDS.

        # To update schema on Main (see also the README.md) use this:
        with helper.schema_update_on_main(op, NEW_TABLES):
            update_schema(op)
        # Update data on Main (and on main only, at least for all replicated tables)
        update_data(op)


def downgrade():
    pass

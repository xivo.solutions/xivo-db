"""Add labels table

Revision ID: cdf4a9c04604
Revises: 2e1abfe11324

"""

# revision identifiers, used by Alembic.

revision = 'cdf4a9c04604'
down_revision = '2e1abfe11324'

import sqlalchemy as sa
from alembic import op

from xivo_db import helper


def upgrade():
    subname = "main_" + helper.get_mds_name_fallback_to_mac()

    if helper.is_mds() and helper.check_subscription_exists(op, subname):
        op.execute("GRANT INSERT, UPDATE, DELETE, TRUNCATE, REFERENCES ON TABLE userfeatures TO asterisk")

    op.create_table(
        'labels',
        sa.Column('id', sa.Integer, nullable=False),
        sa.Column('display_name', sa.String(128), nullable=False),
        sa.Column('description', sa.Text, nullable=True, default=''),
        sa.PrimaryKeyConstraint('id'),
        sa.UniqueConstraint('display_name'),
    )

    op.create_table(
        'userlabels',
        sa.Column('id', sa.Integer, nullable=False),
        sa.Column('user_id', sa.Integer, nullable=False),
        sa.Column('label_id', sa.Integer, nullable=False),
        sa.PrimaryKeyConstraint('id'),
    )

    op.create_foreign_key(constraint_name='userlabels_label_id_fkey', source_table='userlabels',
                          referent_table='labels', local_cols=['label_id'], remote_cols=['id'],
                          ondelete='CASCADE', )
    op.create_foreign_key(constraint_name='userlabels_user_id_fkey', source_table='userlabels',
                          referent_table='userfeatures', local_cols=['user_id'], remote_cols=['id'],
                          ondelete='CASCADE', )

    if helper.is_mds():
        if helper.check_subscription_exists(op, subname):
            op.execute("REVOKE INSERT, UPDATE, DELETE, TRUNCATE, REFERENCES ON TABLE userfeatures FROM asterisk")
            op.execute("REVOKE INSERT, UPDATE, DELETE, TRUNCATE, REFERENCES ON TABLE userlabels FROM asterisk")
            op.execute("REVOKE INSERT, UPDATE, DELETE, TRUNCATE, REFERENCES ON TABLE labels FROM asterisk")
            helper.refresh_publication_deferred()
    else:
        if helper.check_publication_exists(op, "mds"):
            op.execute('ALTER PUBLICATION mds ADD TABLE labels, userlabels;')


def downgrade():
    pass

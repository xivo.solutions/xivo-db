"""add_route_table

Revision ID: a7f322263182
Revises: 884dcf0066bf

"""

# revision identifiers, used by Alembic.
revision = 'a7f322263182'
down_revision = '884dcf0066bf'

import sqlalchemy as sa
from alembic import op


def upgrade():
    op.create_table(
        'route',
        sa.Column('id', sa.Integer, nullable=False),
        sa.Column('pattern', sa.String, nullable=False),
        sa.Column('regexp', sa.String, nullable=True),
        sa.Column('subroutine', sa.String, nullable=True),
        sa.Column('description', sa.Text),
        sa.Column('priority', sa.Integer, nullable=False),
        sa.PrimaryKeyConstraint('id'),
    )

    op.create_table(
        'routecontext',
        sa.Column('routeid', sa.Integer, nullable=False),
        sa.Column('contextname', sa.String(39), nullable=False),
        sa.PrimaryKeyConstraint('routeid', 'contextname', name='routecontext_pkey'),
        sa.ForeignKeyConstraint(('routeid',), ('route.id',), ondelete='RESTRICT'),
        sa.ForeignKeyConstraint(('contextname',), ('context.name',), ondelete='RESTRICT'),
    )

    op.create_table(
        'routemediaserver',
        sa.Column('routeid', sa.Integer, nullable=False),
        sa.Column('mdsid', sa.Integer, nullable=False),
        sa.PrimaryKeyConstraint('routeid', 'mdsid', name='routemediaserver_pkey'),
        sa.ForeignKeyConstraint(('routeid',), ('route.id',), ondelete='RESTRICT'),
        sa.ForeignKeyConstraint(('mdsid',), ('mediaserver.id',), ondelete='RESTRICT'),
    )

    op.create_table(
        'routetrunk',
        sa.Column('routeid', sa.Integer, nullable=False),
        sa.Column('trunkid', sa.Integer, nullable=False),
        sa.Column('priority', sa.Integer, nullable=False),
        sa.PrimaryKeyConstraint('routeid', 'trunkid', name='routetrunk_pkey'),
        sa.ForeignKeyConstraint(('routeid',), ('route.id',), ondelete='RESTRICT'),
        sa.ForeignKeyConstraint(('trunkid',), ('trunkfeatures.id',), ondelete='RESTRICT'),
    )


def downgrade():
    op.drop_table('routetrunk')
    op.drop_table('routemediaserver')
    op.drop_table('routecontext')
    op.drop_table('route')

"""add mediaserver table

Revision ID: d65f39f4f72
Revises: 1fc0ac1a6e64

"""

# revision identifiers, used by Alembic.
revision = 'd65f39f4f72'
down_revision = '1fc0ac1a6e64'

import sqlalchemy as sa
from alembic import op


def upgrade():
    op.create_table(
        'mediaserver',
        sa.Column('id', sa.Integer, nullable=False),
        sa.Column('name', sa.String(128), nullable=False),
        sa.Column('display_name', sa.String(128), nullable=False),
        sa.Column('voip_ip', sa.String(39), nullable=False),
        sa.PrimaryKeyConstraint('id'),
    )

    op.create_unique_constraint('uq_name', 'mediaserver', ["name"])


def downgrade():
    op.drop_table('mediaserver')

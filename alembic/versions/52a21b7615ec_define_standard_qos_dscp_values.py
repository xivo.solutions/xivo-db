"""define_standard_QoS_DSCP_values

Revision ID: 52a21b7615ec
Revises: a49c8eadf58f

"""

# revision identifiers, used by Alembic.
revision = '52a21b7615ec'
down_revision = 'a49c8eadf58f'

import sqlalchemy as sa
from alembic import op

from xivo_db import helper

# List modified/added tables
NEW_TABLES = []
UPDATED_TABLES = []

staticsip_table = sa.sql.table('staticsip',
                               sa.sql.column('commented'),
                               sa.sql.column('var_name'),
                               sa.sql.column('var_val'))


# Function which updates schema (create/update tables)
def update_schema(op):
    pass


def create_query(target_var_name, added_var_val):
    return (staticsip_table.update().where(
        sa.sql.and_(
            staticsip_table.c.var_name == target_var_name,
            staticsip_table.c.var_val == None
        )
    ).values(var_val=added_var_val, commented=0))


def update_data(op):
    if helper.is_mds():
        pass
    else:
        # Update data
        op.execute(create_query('tos_sip', 'AF31'))
        op.execute(create_query('tos_audio', 'EF'))
        pass


def upgrade():
    # Note: If you create a table using sqlalchemy command:
    #  - command will be run as asterisk,
    #  - therefore the owner's object will be asterisk
    #  - and therefore postgresql will give stats user the SELECT privilege

    if helper.is_mds():
        # Add here the actions applicable to MDS.
        # Don't forget that data are replicated from Main to MDS.

        # To update schema on MDS (see also the README.md) use this:
        with helper.schema_update_on_mds(op, NEW_TABLES, UPDATED_TABLES):
            update_schema(op)
        # Do not update **data** on MDS
        pass
    else:
        # Add here the actions applicable to XiVO Main
        # Don't forget that data are replicated from Main to MDS.

        # To update schema on Main (see also the README.md) use this:
        with helper.schema_update_on_main(op, NEW_TABLES):
            update_schema(op)
        # Update data on Main (and on main only, at least for all replicated tables)
        update_data(op)


def downgrade():
    pass

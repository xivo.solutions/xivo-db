"""Add route internal option

Revision ID: 4a22caaf0562
Revises: 1945863164ca

"""

# revision identifiers, used by Alembic.
revision = '4a22caaf0562'
down_revision = '1945863164ca'

import sqlalchemy as sa
from alembic import op


def check_column_exists(table, column_name):
    sql = "SELECT EXISTS (SELECT 1 " \
          "FROM information_schema.columns " \
          "WHERE table_name='" + table + "' and column_name='" + column_name + "')"

    result_proxy = op.get_bind().execute(sql)
    exists = result_proxy.fetchone()[0]

    return exists


def upgrade():
    if not check_column_exists('route', 'internal'):
        # The "internal" column can be already added from "Migrate outcalls to routes"
        op.add_column('route', sa.Column('internal', sa.Boolean, nullable=False, server_default='False'))


def downgrade():
    pass

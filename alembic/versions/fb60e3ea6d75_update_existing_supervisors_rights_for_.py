"""update existing supervisors rights for dissuasion

Revision ID: fb60e3ea6d75
Revises: 245bce609c46

"""

# revision identifiers, used by Alembic.
revision = 'fb60e3ea6d75'
down_revision = '245bce609c46'

from alembic import op

from xivo_db import helper


def upgrade():
    if helper.is_mds():
        pass
    else:
        op.execute(
            "INSERT INTO rights(user_id, category, category_id) SELECT id,'dissuasion_access','0' FROM users WHERE type = 'supervisor'")


def downgrade():
    pass

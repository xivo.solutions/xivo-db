"""Add userpreferences table

Revision ID: f5ed9ff8bd89
Revises: cdf4a9c04604

"""

# revision identifiers, used by Alembic.
revision = 'f5ed9ff8bd89'
down_revision = 'cdf4a9c04604'

import sqlalchemy as sa
from alembic import op

from xivo_db import helper

# List modified/added tables
UPDATED_TABLES = ['userfeatures']
NEW_TABLES = ['userpreferences']


# Function which creates tables/updates schema
def update_schema(op):
    # Create table
    op.create_table(
        'userpreferences',
        sa.Column('user_id', sa.Integer, sa.ForeignKey('userfeatures.id', ondelete='CASCADE'), nullable=False),
        sa.Column('key', sa.String(255), nullable=False),
        sa.Column('value', sa.String(255), nullable=False),
        sa.Column('value_type', sa.String(255), nullable=False, default='String'),
        sa.PrimaryKeyConstraint('user_id', 'key'),
    )


def upgrade():
    if helper.is_mds():
        with helper.schema_update_on_mds(op, NEW_TABLES, UPDATED_TABLES):
            update_schema(op)
    else:
        with helper.schema_update_on_main(op, NEW_TABLES):
            update_schema(op)


def downgrade():
    pass

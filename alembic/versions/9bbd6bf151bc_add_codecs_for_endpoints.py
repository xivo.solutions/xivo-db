"""add codecs for endpoints

Revision ID: 9bbd6bf151bc
Revises: e736e9c74b6a

"""

# revision identifiers, used by Alembic.
revision = '9bbd6bf151bc'
down_revision = 'e736e9c74b6a'

from alembic import op
import sqlalchemy as sa
from xivo_db import helper


# List modified/added tables
NEW_TABLES=[]
UPDATED_TABLES=['staticsip']


def update_data(op):
    if helper.is_mds():
        pass
    else:
        op.execute("UPDATE staticsip SET var_val='all' WHERE var_name='disallow' AND (var_val IS NULL OR var_val = '')")
        op.execute("UPDATE staticsip SET var_val='ulaw,alaw,gsm,h263', commented=0 WHERE var_name='allow' AND (var_val IS NULL OR var_val = '')")


def upgrade():
    if helper.is_mds():
        pass
    else:
        update_data(op)


def downgrade():
    pass

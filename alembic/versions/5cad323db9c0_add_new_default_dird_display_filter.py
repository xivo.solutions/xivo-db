"""Add new default dird display filter

Revision ID: 5cad323db9c0
Revises: 9748432bfa4c

"""

# revision identifiers, used by Alembic.
from xivo_db import helper
import sqlalchemy as sa
from alembic import op
revision = '5cad323db9c0'
down_revision = '9748432bfa4c'


# List modified/added tables
NEW_TABLES = []
UPDATED_TABLES = []

cti_displays = sa.sql.table('ctidisplays',
                            sa.sql.column('description'),
                            sa.sql.column('name'),
                            sa.sql.column('data'))

value = ('{' +
         '"10": [ "Nom","name","","name" ],' +
         '"20": [ "Subtitle1","subtitle1","","title" ],' +
         '"30": [ "Subtitle2","subtitle2","","service" ],' +
         '"40": [ "Téléphone interne","number","","phone" ],' +
         '"50": [ "Téléphone pro","callable","","phone_pro" ],' +
         '"60": [ "Téléphone mobile","callable","","phone_mobile" ],' +
         '"70": [ "Téléphone domicile","callable","","phone_home" ],' +
         '"80": [ "E-mail", "email", "", "email"],' +
         '"90": [ "Télécopie", "callable", "", "fax"],' +
         '"100":[ "Fonction", "info", "", "title"],' +
         '"110":[ "Service", "info", "", "service"],' +
         '"120":[ "Manager", "info", "", "manager"],' +
         '"130":[ "Société", "info", "", "company"],' +
         '"140":[ "Site web", "info", "", "website"],' +
         '"150":[ "Bureau", "info", "", "office"],' +
         '"160":[ "Adresse", "info", "", "location"],' +
         '"170":[ "", "favorite", "", "favorite"],' +
         '"180":[ "", "personal", "", ""],' +
         '"190":[ "Avatar", "picture", "", "picture"]}')


def add_new_dird_display():
    op.execute(cti_displays
               .update()
               .where(cti_displays.c.name == 'Display')
               .values(name='DisplayLegacy',
                       description='Ancien affichage'))
    op.execute(cti_displays
               .insert()
               .values(name='Display',
                       data=value,
                       description='Affichage par défaut'))


# Function which updates schema (create/update tables)
def update_schema(op):
    # Create/Update table
    # Add types ...
    # Add foreing keys
    pass


def update_data(op):
    if helper.is_mds():
        pass
    else:
        # Update data
        add_new_dird_display()


def upgrade():
    # Note: If you create a table using sqlalchemy command:
    #  - command will be run as asterisk,
    #  - therefore the owner's object will be asterisk
    #  - and therefore postgresql will give stats user the SELECT privilege

    if helper.is_mds():
        # Add here the actions applicable to MDS.
        # Don't forget that data are replicated from Main to MDS.

        # To update schema on MDS (see also the README.md) use this:
        with helper.schema_update_on_mds(op, NEW_TABLES, UPDATED_TABLES):
            update_schema(op)
        # Do not update **data** on MDS
        pass
    else:
        # Add here the actions applicable to XiVO Main
        # Don't forget that data are replicated from Main to MDS.

        # To update schema on Main (see also the README.md) use this:
        with helper.schema_update_on_main(op, NEW_TABLES):
            update_schema(op)
        # Update data on Main (and on main only, at least for all replicated tables)
        update_data(op)


def downgrade():
    pass

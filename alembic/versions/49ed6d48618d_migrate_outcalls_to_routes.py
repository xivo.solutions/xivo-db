"""Migrate outcalls to routes

Revision ID: 49ed6d48618d
Revises: a8ced70936d4

"""


# revision identifiers, used by Alembic.

revision = '49ed6d48618d'
down_revision = 'a8ced70936d4'

import sqlalchemy as sa
from alembic import op

route_table = sa.sql.table('route',
                           sa.Column('id'),
                           sa.Column('pattern'),
                           sa.Column('regexp'),
                           sa.Column('subroutine'),
                           sa.Column('description'),
                           sa.Column('priority'),
                           sa.Column('internal')
                           )

routecontext_table = sa.sql.table('routecontext',
                                  sa.Column('routeid'),
                                  sa.Column('contextname')
                                  )

routemediaserver_table = sa.sql.table('routemediaserver',
                                      sa.Column('routeid'),
                                      sa.Column('mdsid')
                                      )

routetrunk_table = sa.sql.table('routetrunk',
                                sa.Column('routeid'),
                                sa.Column('trunkid'),
                                sa.Column('priority')
                                )

outcall_table = sa.sql.table('outcall',
                             sa.sql.column('id'),
                             sa.sql.column('name'),
                             sa.sql.column('context'),
                             sa.sql.column('internal'),
                             sa.sql.column('preprocess_subroutine'),
                             sa.sql.column('description'),
                             sa.sql.column('commented')
                             )

outcalltrunk_table = sa.sql.table('outcalltrunk',
                                  sa.sql.column('id'),
                                  sa.sql.column('outcallid'),
                                  sa.sql.column('trunkfeaturesid'),
                                  sa.sql.column('priority')
                                  )

extensions_table = sa.sql.table('extensions',
                                sa.sql.column('commented'),
                                sa.sql.column('context'),
                                sa.sql.column('exten'),
                                sa.sql.column('type'),
                                sa.sql.column('typeval'),
                                )

dialpattern_table = sa.sql.table('dialpattern',
                                 sa.sql.column('type'),
                                 sa.sql.column('typeid'),
                                 sa.sql.column('externprefix'),
                                 sa.sql.column('exten'),
                                 sa.sql.column('stripnum'),
                                 sa.sql.column('callerid')
                                 )

routepattern_table = sa.sql.table('routepattern',
                                  sa.sql.column('id'),
                                  sa.sql.column('pattern'),
                                  sa.sql.column('regexp'),
                                  sa.sql.column('target'),
                                  sa.sql.column('callerid'),
                                  sa.sql.column('routeid')
                                  )

trunkfeatures_table = sa.sql.table('trunkfeatures',
                                   sa.sql.column('id')
                                   )

context_table = sa.sql.table('context',
                             sa.sql.column('name'),
                             sa.sql.column('contexttype')
                             )

contextinclude_table = sa.sql.table('contextinclude',
                                    sa.sql.column('context'),
                                    sa.sql.column('include')
                                    )

schedule_table = sa.sql.table('schedule',
                              sa.sql.column('id'),
                              sa.sql.column('name')
                              )

schedule_path_table = sa.sql.table('schedule_path',
                                   sa.sql.column('schedule_id'),
                                   sa.sql.column('path'),
                                   sa.sql.column('pathid')
                                   )

rightcall_table = sa.sql.table('rightcall',
                               sa.sql.column('id'),
                               sa.sql.column('name')
                               )

rightcallmember_table = sa.sql.table('rightcallmember',
                                     sa.sql.column('rightcallid'),
                                     sa.sql.column('type'),
                                     sa.sql.column('typeval')
                                     )

mapping = (
    ("_", ""),
    ("+", "\+"),
    ("X", "\d"),
    ("Z", "[1-9]"),
    ("N", "[2-9]"),
    (".", "[0-9#\*]+"),
    ("!", "[0-9#\*]*")
)


def delete_spurious_outcalltrunks():
    # Deleted trunks may still be referenced in the outcalltrunk table
    current_trunkfeatures_id_query = sa.sql.select([trunkfeatures_table.c.id])
    run_request(outcalltrunk_table
        .delete()
        .where(
        outcalltrunk_table.c.trunkfeaturesid.notin_(current_trunkfeatures_id_query)
    )
    )


def get_outcalls():
    outcalls = run_request(sa.sql.select([
        outcall_table.c.id, outcall_table.c.name, outcall_table.c.context, outcall_table.c.internal,
        outcall_table.c.preprocess_subroutine, outcall_table.c.description, outcall_table.c.commented,
    ]))

    return outcalls


def get_max_route_prio():
    return run_request(sa.sql.select([sa.func.coalesce(sa.func.max(route_table.c.priority) + 1, 1)])).scalar()


def transform_single_pattern(pattern):
    for (key, val) in mapping:
        pattern = pattern.replace(key, val)
    return pattern


def transform_stripnum_to_regexp(stripnum):
    result = "(.*)"
    if stripnum > 0:
        result = ".{" + str(stripnum) + "}" + result
    return result


def transform_prefix_to_target(prefix):
    return prefix + "\\1"


def get_regexp_and_target_for_dialpattern(dialpattern):
    regexp = None
    target = None
    if dialpattern['externprefix'] and dialpattern['stripnum']:
        regexp = transform_stripnum_to_regexp(dialpattern['stripnum'])
        target = transform_prefix_to_target(dialpattern['externprefix'])
    return regexp, target


def get_dialpatterns_for_outcall(outcall_id):
    dialpatterns = run_request(sa.sql.select([
        dialpattern_table.c.exten,
        dialpattern_table.c.externprefix,
        dialpattern_table.c.stripnum,
        dialpattern_table.c.type,
        dialpattern_table.c.typeid,
        dialpattern_table.c.callerid
    ])
        .where(sa.sql.and_(
        dialpattern_table.c.type == 'outcall',
        dialpattern_table.c.typeid == outcall_id))
    )

    # Create a list of dict out of the ResultProxy fetchall() returns
    return [{column: value for column, value in rowproxy.items()} for rowproxy in dialpatterns.fetchall()]


def migrate_dialpatterns_to_routepatterns(route_id, dialpatterns_list):
    for dialpattern in dialpatterns_list:
        pattern = transform_single_pattern(dialpattern['exten'])
        regexp, target = get_regexp_and_target_for_dialpattern(dialpattern)
        run_request(routepattern_table
                    .insert()
                    .values(pattern=pattern,
                            regexp=regexp,
                            target=target,
                            callerid=dialpattern['callerid'],
                            routeid=route_id))


def get_trunk_for_outcall(outcall_id):
    trunks = run_request(sa.sql.select([
        outcalltrunk_table.c.outcallid,
        outcalltrunk_table.c.trunkfeaturesid,
        outcalltrunk_table.c.priority
    ])
                         .where(outcalltrunk_table.c.outcallid == outcall_id)
                         )

    # Create a list of dict out of the ResultProxy fetchall() returns
    return [{column: value for column, value in rowproxy.items()} for rowproxy in trunks.fetchall()]


def create_single_route(outcall, priority, bool_internal):
    if outcall.name and outcall.description:
        description = "%s: %s" % (outcall.name, outcall.description)
    else:
        description = "%s%s" % (outcall.name, outcall.description)

    return run_request(route_table
                       .insert()
                       .returning(route_table.c.id)
                       .values(subroutine=outcall.preprocess_subroutine,
                               description=description,
                               priority=priority,
                               internal=bool_internal))


def insert_context_for_route(route_id, context):
    run_request(routecontext_table
                .insert()
                .values(routeid=route_id,
                        contextname=context))


def insert_trunks_for_route(route_id, trunks):
    for trunk in trunks:
        run_request(routetrunk_table
                    .insert()
                    .values(routeid=route_id,
                            trunkid=trunk['trunkfeaturesid'],
                            priority=trunk['priority']))


def delete_outcall_extensions():
    run_request(extensions_table
                .delete()
                .where(extensions_table.c.type == 'outcall'))


def drop_dialpattern():
    op.drop_table('dialpattern')


def drop_outcalltrunk():
    op.drop_table('outcalltrunk')


def drop_outcall():
    op.drop_table('outcall')


def run_request(request):
    return op.get_bind().execute(request)


def rename_outcall_to_route_in_enums():
    op.execute("ALTER TYPE schedule_path_type RENAME VALUE \'outcall\' TO \'route\'")
    op.execute("ALTER TYPE rightcallmember_type RENAME VALUE \'outcall\' TO \'route\'")


def find_internal_contexts_including_outcall_context(outcall_context):
    join_conditions = (contextinclude_table
                       .join(context_table,
                             sa.sql.and_(
                                 context_table.c.name == contextinclude_table.c.context,
                                 context_table.c.contexttype == 'internal')))

    query = (sa.sql.select([contextinclude_table.c.context],
                           from_obj=[join_conditions])
             .where(contextinclude_table.c.include == outcall_context))

    rows = run_request(query)

    return [row[contextinclude_table.c.context] for row in rows.fetchall()]


def get_schedule_of_outcall(outcall_id):
    join_conditions = (schedule_table
                       .join(schedule_path_table,
                             schedule_table.c.id == schedule_path_table.c.schedule_id))

    query = (sa.sql.select([schedule_table.c.name],
                           from_obj=[join_conditions])
        .where(sa.sql.and_(
        schedule_path_table.c.path == 'outcall',
        schedule_path_table.c.pathid == outcall_id
    )))

    schedule_name = run_request(query).scalar()
    return schedule_name


def deassociate_schedule_from_dropped_outcall(outcall_id):
    run_request(schedule_path_table
        .delete()
        .where(sa.sql.and_(
        schedule_path_table.c.path == 'outcall',
        schedule_path_table.c.pathid == outcall_id)))


def get_rights_of_outcall(outcall_id):
    join_conditions = (rightcall_table
                       .join(rightcallmember_table,
                             rightcall_table.c.id == rightcallmember_table.c.rightcallid))

    query = (sa.sql.select([rightcall_table.c.name],
                           from_obj=[join_conditions])
        .where(sa.sql.and_(
        rightcallmember_table.c.type == 'outcall',
        rightcallmember_table.c.typeval == str(outcall_id)
    )))

    rightcall_names = run_request(query)
    return [rightcall_name[rightcall_table.c.name] for rightcall_name in rightcall_names.fetchall()]


def deassociate_rights_from_dropped_outcall(outcall_id):
    run_request(rightcallmember_table
        .delete()
        .where(sa.sql.and_(
        rightcallmember_table.c.type == 'outcall',
        rightcallmember_table.c.typeval == str(outcall_id))))


def _get_lengthiest_outcall_name():
    outcall_name_max_length = run_request(sa.sql.select([sa.func.length(outcall_table.c.name)])
                                          .order_by(sa.desc(sa.func.length(outcall_table.c.name)))
                                          ).scalar()

    return outcall_name_max_length if outcall_name_max_length else 1


def _print_log(msg_prefix, msg_depth, msg, prefix='[MIGRATE_OUTCALL]', log_level='INFO'):
    default_log_level = 'INFO'
    known_log_level = [default_log_level, 'WARNING']
    print(u'{0} - {1:{level_maxwidth}} - {2:{msgpfix_maxwidth}}: {3}{4}'
          .format(prefix,
                  log_level if log_level in known_log_level else default_log_level,
                  msg_prefix,
                  '\t' * msg_depth,
                  msg,
                  level_maxwidth=len(max(known_log_level, key=len)),
                  msgpfix_maxwidth=OUTCALL_MAXLENGTH if len(msg_prefix) > 0 else 1))


def print_dialpatterns(outcall_name, dialpatterns):
    for dialpattern in dialpatterns:
        _print_log(outcall_name, 2,
                   '- exten: "{}", externprefix: "{}", stripnum: "{}", callerid: "{}"'.format(
                       dialpattern['exten'],
                       dialpattern['externprefix'],
                       dialpattern['stripnum'],
                       dialpattern['callerid']))


def print_context(outcall_name, outcall_context):
    _print_log(outcall_name, 2, '- context "{}"'.format(outcall_context))


def print_internal(outcall_name, bool_internal):
    _print_log(outcall_name, 2, '- internal "{}"'.format(bool_internal))


def print_trunks(outcall_name, trunks):
    _print_log(outcall_name, 1, 'Outcall was using trunks:')
    for trunk in trunks:
        _print_log(outcall_name, 2,
                   '- trunkfeatures.id {} with priority {}'.format(trunk['trunkfeaturesid'], trunk['priority']))


def print_schedule(outcall_name, schedule_name):
    _print_log(outcall_name, 2,
               '- schedule: "{}"'.format(schedule_name))


def print_rights(outcall_name, rights_schedule):
    _print_log(outcall_name, 1, 'Outcall had the following rights applied:')
    for right_name in rights_schedule:
        _print_log(outcall_name, 2,
                   '- right: "{}"'.format(right_name))


def print_outcall(outcall_name, outcall_preprocess_subroutine, dialpatterns, outcall_context, internal, trunks,
                  outcall_schedule, rights_schedule):
    _print_log('', 0, 'Migrating outcall "{}" to route'.format(outcall_name))
    _print_log(outcall_name, 1, 'Outcall definition was:')

    print_dialpatterns(outcall_name, dialpatterns)
    _print_log(outcall_name, 2,
               '- subroutine: "{}"'.format(outcall_preprocess_subroutine))
    print_context(outcall_name, outcall_context)
    print_internal(outcall_name, internal)
    print_schedule(outcall_name, outcall_schedule)
    print_trunks(outcall_name, trunks)
    print_rights(outcall_name, rights_schedule)


OUTCALL_MAXLENGTH = 1


def upgrade():
    op.add_column('route', sa.Column('internal', sa.Boolean, nullable=False, server_default='False'))

    # Var needed for the _print_log function (to have the correct padding)
    global OUTCALL_MAXLENGTH
    OUTCALL_MAXLENGTH = _get_lengthiest_outcall_name()

    delete_spurious_outcalltrunks()

    op.drop_column('route', 'pattern')
    op.drop_column('route', 'regexp')

    op.create_table(
        'routepattern',
        sa.Column('id', sa.Integer, nullable=False),
        sa.Column('pattern', sa.String, nullable=False),
        sa.Column('regexp', sa.String, nullable=True),
        sa.Column('target', sa.String, nullable=True),
        sa.Column('callerid', sa.String(80), nullable=True),
        sa.Column('routeid', sa.Integer, nullable=False),
        sa.PrimaryKeyConstraint('id'),
        sa.ForeignKeyConstraint(('routeid',), ('route.id',), ondelete='RESTRICT'),
    )

    _print_log('', 0, 'Outcalls are going to be migrated to Route definition')

    prio = get_max_route_prio()
    for outcall in get_outcalls():
        # Get outcall info
        dialpatterns_list = get_dialpatterns_for_outcall(outcall.id)
        contexts_list = find_internal_contexts_including_outcall_context(outcall.context)
        trunks_list = get_trunk_for_outcall(outcall.id)
        outcall_schedule = get_schedule_of_outcall(outcall.id)
        rights_schedule = get_rights_of_outcall(outcall.id)
        bool_internal = True if outcall.internal else False

        # Print outcall infos
        print_outcall(outcall.name,
                      outcall.preprocess_subroutine,
                      dialpatterns_list,
                      outcall.context,
                      bool_internal,
                      trunks_list,
                      outcall_schedule if outcall_schedule else 'None',
                      rights_schedule)

        # Migrate outcall to route
        if outcall.commented == 0:
            # Initiate route
            route_id = create_single_route(outcall, prio, bool_internal).scalar()

            # Migration
            # - dialpattern to routepattern
            migrate_dialpatterns_to_routepatterns(route_id, dialpatterns_list)

            # - outgoing context to internal(s) context(s)
            if contexts_list:
                _print_log(outcall.name, 1, 'New route will be applied to the following **internal** contexts')
                for context in contexts_list:
                    _print_log(outcall.name, 2, '- "{}"'.format(context))
                    insert_context_for_route(route_id, context)
            else:
                _print_log(outcall.name, 1,
                           'Could not find an internal context matching the outgoing context "{}"'.format(
                               outcall.context), log_level='WARNING')
                _print_log(outcall.name, 1, 'Falling back on context "{}" to create the route'.format(outcall.context),
                           log_level='WARNING')
                _print_log(outcall.name, 1,
                           'You HAVE TO change it manually after the upgrade to the correct internal context',
                           log_level='WARNING')
                _print_log(outcall.name, 1, 'THIS ROUTE WON\'T WORK UNTIL YOU FIX THE CONTEXT CONFIGURATION',
                           log_level='WARNING')
                insert_context_for_route(route_id, outcall.context)

            # - trunks
            insert_trunks_for_route(route_id, trunks_list)

            prio = prio + 1
        else:
            _print_log(outcall.name, 0, 'Outcall WAS NOT MIGRATED TO ROUTE because it is deactivated ib db',
                       log_level='WARNING')
            _print_log(outcall.name, 0,
                       'If you want to keep it you have to create it manually (see previous outcall definition above)',
                       log_level='WARNING')

            if outcall_schedule:
                deassociate_schedule_from_dropped_outcall(outcall.id)

            if rights_schedule:
                deassociate_rights_from_dropped_outcall(outcall.id)

    rename_outcall_to_route_in_enums()
    delete_outcall_extensions()
    drop_dialpattern()
    drop_outcalltrunk()
    drop_outcall()


def downgrade():
    pass

"""add_default_joinempty_leavewhenempty_for_group_queues

Revision ID: 9748432bfa4c
Revises: 88f350be1fa7

"""

# revision identifiers, used by Alembic.
revision = '9748432bfa4c'
down_revision = '88f350be1fa7'

from alembic import op
import sqlalchemy as sa
from xivo_db import helper


# List modified/added tables
NEW_TABLES=[]
UPDATED_TABLES=[]

def upgrade():
    if helper.is_mds():
        pass
    else:
        op.execute(
            '''UPDATE queue SET joinempty='unavailable,invalid,unknown', leavewhenempty='unavailable,invalid,unknown' WHERE category='group' ''')


def downgrade():
    if helper.is_mds():
        pass
    else:
        op.execute(
            '''UPDATE queue SET joinempty='', leavewhenempty='' WHERE category='group' ''')
    

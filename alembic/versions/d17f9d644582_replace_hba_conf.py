"""Replace hba conf

Revision ID: d17f9d644582
Revises: 420ed0e31a38

"""

# revision identifiers, used by Alembic.
revision = 'd17f9d644582'
down_revision = '420ed0e31a38'

import filecmp
import os
import shutil

from xivo_db import helper


def upgrade():
    existing_conf = os.path.join(os.environ["PGDATA"], "pg_hba.conf")
    backup_conf = os.path.join(os.environ["PGDATA"], "pg_hba.conf.bak")
    new_conf = "/etc/postgresql/pg_hba.conf"

    print(u"""[MIGRATE_HBA_CONF] : ***********************************************************************
[MIGRATE_HBA_CONF] : WARNING: Restricting database access
[MIGRATE_HBA_CONF] :
[MIGRATE_HBA_CONF] : New /var/lib/postgresql/11/main/pg_hba.conf file will be instaled.""")

    if helper.is_mds():
        print(u"[MIGRATE_HBA_CONF] : It is not needed to edit this file on media servers.")
    else:
        print(u"""[MIGRATE_HBA_CONF] : It must be edited manually to allow connections from remote components.
[MIGRATE_HBA_CONF] :
[MIGRATE_HBA_CONF] : If you have XiVO CC installed, add this entry:
[MIGRATE_HBA_CONF] :     host    asterisk      all      XIVOCC_HOST/32      md5
[MIGRATE_HBA_CONF] :
[MIGRATE_HBA_CONF] : Add entries for every media server:
[MIGRATE_HBA_CONF] :     host    asterisk      all      MDS_HOST/32         md5
[MIGRATE_HBA_CONF] :
[MIGRATE_HBA_CONF] : Add entry for high availability on slave XiVO:
[MIGRATE_HBA_CONF] :     host    asterisk    postgres   MASTER_VOIP_IP/32   trust
[MIGRATE_HBA_CONF] :
[MIGRATE_HBA_CONF] : Apply the settings by command "xivo-dcomp reload-db".""")

    if not filecmp.cmp(new_conf, existing_conf):
        # Upgrading from postgres 11
        print(u"[MIGRATE_HBA_CONF] : ")
        print(u"[MIGRATE_HBA_CONF] : Old pg_hba.conf file will be renamed to .bak")
        shutil.move(existing_conf, backup_conf)
        shutil.copy(new_conf, existing_conf)
    else:
        # Upgrading from postgres 9.4 (new hba file is already installed by docker-entrypoint.sh)
        print("""[MIGRATE_HBA_CONF] :
[MIGRATE_HBA_CONF] : Compare the settings with the old pg_hba.conf that was saved to folder:
[MIGRATE_HBA_CONF] : /var/tmp/xivo-migrate-db-94-to-11/postgresql-94-conf-backup/""")

    print(u"[MIGRATE_HBA_CONF] : ***********************************************************************")


def downgrade():
    pass

"""add_ivr_tables

Revision ID: 38c4a79d9abe
Revises: c1718ed0b1b1

"""

# revision identifiers, used by Alembic.
from xivo_db import helper

revision = '38c4a79d9abe'
down_revision = 'c1718ed0b1b1'

from alembic import op
from sqlalchemy import UniqueConstraint, column, Numeric
from sqlalchemy.schema import (Column, ForeignKeyConstraint, PrimaryKeyConstraint)
from sqlalchemy.types import (Integer, String, Text)

# List modified/added tables
NEW_TABLES = []
UPDATED_TABLES = []


# Function which updates schema (create/update tables)
def update_schema(op):
    op.execute('CREATE SCHEMA IF NOT EXISTS ivr')

    op.create_table(
        'ivredit_flowchart',
        Column('id', Integer, autoincrement=True, nullable=False),
        Column('name', String(50), nullable=False),
        PrimaryKeyConstraint('id'),
        UniqueConstraint('name'),
        schema='ivr',
    )

    op.create_check_constraint(
        "ivredit_flowchart_name_empty_string",
        "ivredit_flowchart",
        column('name') != '',
        'ivr'
    )

    op.create_table(
        'ivredit_node',
        Column('id', Integer, autoincrement=True, nullable=False),
        Column('id_flowchart', Integer, nullable=False),
        Column('node_number', Integer, nullable=False),
        Column('node_type', String(20), nullable=False),
        Column('name', String(50), nullable=False),
        PrimaryKeyConstraint('id'),
        ForeignKeyConstraint(
            ('id_flowchart',),
            ('ivr.ivredit_flowchart.id',),
            name='ivredit_node_id_flowchart_id_fkey',
            ondelete='CASCADE',
        ),
        UniqueConstraint('id_flowchart', 'name'),
        UniqueConstraint('id_flowchart', 'node_number'),
        schema='ivr',
    )

    op.create_check_constraint(
        "ivredit_node_name_empty_string",
        "ivredit_node",
        column('name') != '',
        'ivr'
    )

    op.create_table(
        'ivredit_nodecon',
        Column('id', Integer, autoincrement=True, nullable=False),
        Column('id_flowchart', Integer, nullable=False),
        Column('src', Integer, nullable=False),
        Column('src_opt', String(10), nullable=False),
        Column('dst', Integer, nullable=False),
        PrimaryKeyConstraint('id'),
        ForeignKeyConstraint(
            ('id_flowchart', 'src',),
            ('ivr.ivredit_node.id_flowchart', 'ivr.ivredit_node.node_number',),
            name='ivredit_nodecon_src_fkey',
            ondelete='CASCADE',
        ),
        ForeignKeyConstraint(
            ('id_flowchart', 'dst',),
            ('ivr.ivredit_node.id_flowchart', 'ivr.ivredit_node.node_number',),
            name='ivredit_nodecon_dst_fkey',
            ondelete='CASCADE',
        ),
        UniqueConstraint('id_flowchart', 'src', 'src_opt'),
        schema='ivr',
    )

    op.create_table(
        'ivredit_nodepos',
        Column('id_node', Integer, autoincrement=True, nullable=False),
        Column('x', Integer, nullable=False),
        Column('y', Integer, nullable=False),
        PrimaryKeyConstraint('id_node'),
        ForeignKeyConstraint(
            ('id_node',),
            ('ivr.ivredit_node.id',),
            name='ivredit_nodepos_id_fkey',
            ondelete='CASCADE',
        ),
        schema='ivr',
    )

    op.create_table(
        'ivredit_nodeprm',
        Column('id', Integer, autoincrement=True, nullable=False),
        Column('id_node', Integer, nullable=False),
        Column('prm_name', String(50), nullable=False),
        Column('prm_value', String(255), nullable=False),
        PrimaryKeyConstraint('id'),
        ForeignKeyConstraint(
            ('id_node',),
            ('ivr.ivredit_node.id',),
            name='ivredit_nodeprm_id_fkey',
            ondelete='CASCADE',
        ),
        UniqueConstraint('id_node', 'prm_name'),
        schema='ivr',
    )

    op.create_check_constraint(
        "ivredit_nodeprm_prm_name_empty_string",
        "ivredit_nodeprm",
        column('prm_name') != '',
        'ivr'
    )

    op.create_table(
        'ivredit_voiceprompt',
        Column('id', Integer, autoincrement=True, nullable=False),
        Column('name', String(50), nullable=False),
        Column('source', String(20), nullable=False),
        Column('msg', Text),
        Column('lang', String(2)),
        Column('speed', Numeric(2, 1), nullable=False, server_default='1.0'),
        PrimaryKeyConstraint('id'),
        UniqueConstraint('name'),
        schema='ivr',
    )

    op.create_check_constraint(
        "ivredit_voiceprompt_name_empty_string",
        "ivredit_voiceprompt",
        column('name') != '',
        'ivr'
    )


def update_data(op):
    pass


def upgrade():
    if helper.is_mds():
        pass
    else:
        update_schema(op)


def downgrade():
    pass

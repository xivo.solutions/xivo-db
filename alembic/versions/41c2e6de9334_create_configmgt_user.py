"""create configmgt user

Revision ID: 41c2e6de9334
Revises: 424a6278a5e1

"""
from alembic import op

from xivo_db import postgres

# revision identifiers, used by Alembic.
revision = '41c2e6de9334'
down_revision = '424a6278a5e1'


def upgrade():
    create_user_configmgt()
    op.execute("GRANT SELECT ON ALL TABLES IN SCHEMA PUBLIC TO configmgt")
    op.execute("GRANT ALL ON DATABASE asterisk TO configmgt")


def downgrade():
    pass


def create_user_configmgt():
    db_user = 'configmgt'
    db_user_password = 'configmgt'

    conn = postgres.create_db_connection()
    postgres.create_user(conn, db_user, db_user_password)

"""cti-definitions-bookworm

Revision ID: 29b7e27631d4
Revises: fd59c9f45a31

"""

import logging
from typing import List
import sqlalchemy as sa

from alembic import op
from xivo_db import helper


# revision identifiers, used by Alembic.
revision = '29b7e27631d4'
down_revision = 'fd59c9f45a31'

logging.basicConfig()
logger = logging.getLogger('cti.definitions.bookworm')
logger.setLevel(logging.INFO)

# List modified/added tables
NEW_TABLES = []
UPDATED_TABLES = []


ctidirectories: sa.Table = sa.sql.table('ctidirectories',
                                        sa.sql.column('id'),
                                        sa.sql.column('name'),
                                        sa.sql.column('uri'),
                                        sa.sql.column('delimiter'),
                                        sa.sql.column('match_direct'),
                                        sa.sql.column('match_reverse'),
                                        sa.sql.column('description'),
                                        sa.sql.column('deletable')
                                        )
ctidirectoryfields: sa.Table = sa.sql.table('ctidirectoryfields',
                                            sa.sql.column('dir_id'),
                                            sa.sql.column('fieldname'),
                                            sa.sql.column('value')
                                            )

def fetch_directory(directory_name: str, op) -> int | None:
    """
    retrieves id of selected directory, creates it if missing

    Args:
        directory_name (str): "internal" or "xivodir" from table ctidirectories
        op : sqlalchemy op system

    Returns:
        int: dir_id
    """
    query = (
        sa.sql.select([
            ctidirectories.c.id])
        .where(
            ctidirectories.c.name == directory_name))
    return op.get_bind().execute(query).scalar()


internal_expected_fields: dict[str, str] = {
    'header_1_info': '{firstname} {lastname}',
    'contact_5_email': '{email}',
    'contact_1_callable': '{exten}',
    'contact_3_callable': '{mobile_phone_number}',
    'reverse': '{firstname} {lastname}'
}


xivodir_expected_fields: dict[str, str] = {
    'general_4_info': '{phonebook.society}',
    'contact_5_email': '{phonebook.email}',
    'header_1_info': '{phonebook.displayname}',
    'contact_1_callable': '{phonebooknumber.office.number}',
    'contact_4_callable': '{phonebooknumber.home.number}',
    'contact_3_callable': '{phonebooknumber.mobile.number}',
    'contact_2_callable': '{phonebooknumber.other.number}',
    'reverse': '{phonebook.displayname}'
}


def select_directory_field(dir_id: int, fieldname: str, op) -> int | None:
    """retrieves dir_id of ctidirectoryfields line matching given dir_id and fieldname if there is one

    Args:
        dir_id (int): ctidirectoryfields dir_id to search
        fieldname (str): ctidirectoryfields fieldname to search
        op (_type_): sqlalchemy op system

    Returns:
        int | None: dird_id if there was a line
    """
    query = (
        sa.sql.select([
            ctidirectoryfields])
        .where(
            sa.sql.and_(
                ctidirectoryfields.c.dir_id == dir_id,
                ctidirectoryfields.c.fieldname == fieldname)
        ))
    return op.get_bind().execute(query).scalar()


def construct_directoryfields_rows(directory_id: int, field_map: dict[str, str], op) -> List[dict[str, str | int]]:
    """create the rows to bulk insert for each field that wasn't present

    Args:
        directory_id (int):
        field_map (dict[str, str]):
        op (_type_): sqlalchemy op system

    Returns:
        List[dict[str, str | int]]: the rows to insert
    """
    rows = []
    for fieldname, value in field_map.items():
        if select_directory_field(directory_id, fieldname, op) is None:
            rows.append({'dir_id': directory_id,
                         'fieldname': fieldname,
                         'value': value})
    return rows


def populate_directory_fields(directory_name: str, directory_id: int, op):
    if directory_name == "internal":
        rows = construct_directoryfields_rows(directory_id, internal_expected_fields, op)
    if directory_name == "xivodir":
        rows = construct_directoryfields_rows(directory_id, xivodir_expected_fields, op)
    if len(rows) > 0:
        op.bulk_insert(
            ctidirectoryfields,
            rows
        )


def update_schema(op):
    # Create/Update table
    # Add types ...
    # Add foreing keys
    pass


def update_data(op):
    if helper.is_mds():
        pass
    else:
        # Update data
        xivodir_id = fetch_directory('xivodir', op)
        if xivodir_id is not None:
            populate_directory_fields('xivodir', xivodir_id, op)
        internal_id = fetch_directory('internal', op)
        if internal_id is not None:
            populate_directory_fields('internal', internal_id, op)


def upgrade():
    # Note: If you create a table using sqlalchemy command:
    #  - command will be run as asterisk,
    #  - therefore the owner's object will be asterisk
    #  - and therefore postgresql will give stats user the SELECT privilege

    if helper.is_mds():
        # Add here the actions applicable to MDS.
        # Don't forget that data are replicated from Main to MDS.

        # To update schema on MDS (see also the README.md) use this:
        with helper.schema_update_on_mds(op, NEW_TABLES, UPDATED_TABLES):
            update_schema(op)
        # Do not update **data** on MDS
    else:
        # Add here the actions applicable to XiVO Main
        # Don't forget that data are replicated from Main to MDS.

        # To update schema on Main (see also the README.md) use this:
        with helper.schema_update_on_main(op, NEW_TABLES):
            update_schema(op)
        # Update data on Main (and on main only, at least for all replicated tables)
        update_data(op)


def downgrade():
    pass

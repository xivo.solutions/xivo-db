"""Update webrtc to yes

Revision ID: 5070c2415d6f
Revises: fb60e3ea6d75

"""

# revision identifiers, used by Alembic.
revision = '5070c2415d6f'
down_revision = 'fb60e3ea6d75'

from alembic import op
from sqlalchemy import sql
from sqlalchemy.dialects.postgresql import ARRAY, VARCHAR

from xivo_db import helper

usersip_table = sql.table('usersip',
                          sql.column('id'),
                          sql.column('options'))


def get_partly_filtered_webrtc_lines():
    sql = """
        SELECT
            id, name, options FROM usersip
        WHERE
            'webrtc' = ANY (options)
        """

    result_proxy = op.get_bind().execute(sql)

    return result_proxy


def update_sip_options(sip_id, options):
    op.execute(usersip_table
               .update()
               .values(
        options=sql.cast(options, ARRAY(VARCHAR)))
               .where(usersip_table.c.id == sip_id))


def contains_invalid_webrtc_option(options):
    if any(
            key == 'webrtc'
            and value != 'yes'
            for key, value in options
    ):
        return True
    else:
        return False


def update_sip_option(sip_id, name, options):
    updated_options = []
    for option in options:
        if option[0] != 'webrtc':
            updated_options.append(option)
        else:
            print('[MIGRATE_WEBRTC] : Updating sip line %s (%s) webrtc option from "%s" to "yes"' % (
            sip_id, name, option[1]))
            updated_options.append([u'webrtc', u'yes'])

    update_sip_options(sip_id, updated_options)


def upgrade():
    if helper.is_mds():
        pass
    else:
        rows = get_partly_filtered_webrtc_lines()
        if rows.rowcount > 0:
            for sip_id, name, options in rows:
                if contains_invalid_webrtc_option(options):
                    update_sip_option(sip_id, name, options)


def downgrade():
    pass

"""Add db replication between mds0 and mdsN

Revision ID: c4fab49306c0
Revises: 49ed6d48618d

"""

# revision identifiers, used by Alembic.
revision = 'c4fab49306c0'
down_revision = '49ed6d48618d'

from alembic import op

from xivo_db import helper
from xivo_db import postgres


def _upgrade_mds():
    helper.setup_subscriber_deferred()


def _upgrade_master():
    sql_commands = helper.get_callisto_publisher_sql()
    for cmd in sql_commands:
        run_request(cmd)


def run_request(request):
    return op.execute(request)


def upgrade():
    postgres.add_replication_to_asterisk()
    if helper.is_mds():
        _upgrade_mds()
    else:
        _upgrade_master()


def downgrade():
    pass

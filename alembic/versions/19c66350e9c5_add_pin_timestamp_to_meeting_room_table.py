"""Add pin timestamp to meeting room table

Revision ID: 19c66350e9c5
Revises: addb1d5f2e27

"""

# revision identifiers, used by Alembic.
from sqlalchemy import TIMESTAMP, Column

revision = '19c66350e9c5'
down_revision = 'addb1d5f2e27'

from alembic import op
import sqlalchemy as sa
from xivo_db import helper

# List modified/added tables
NEW_TABLES = []
UPDATED_TABLES = ['meetingroom']


# Function which updates schema (create/update tables)
def update_schema(op):
    op.add_column('meetingroom',
                  Column('pin_timestamp', TIMESTAMP, nullable=False,
                         server_default=sa.text("(current_timestamp at time zone 'utc')")))


def update_data(op):
    if helper.is_mds():
        pass
    else:
        # Update data
        pass


def upgrade():
    if helper.is_mds():
        with helper.schema_update_on_mds(op, NEW_TABLES, UPDATED_TABLES):
            update_schema(op)
        pass
    else:
        with helper.schema_update_on_main(op, NEW_TABLES):
            update_schema(op)
        update_data(op)


def downgrade():
    pass

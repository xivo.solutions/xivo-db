"""4388-fix-meetingroom-table-unique-constraint

Revision ID: 01c574702284
Revises: 533bdd6f24ed

"""

from alembic import op

# revision identifiers, used by Alembic.
from xivo_db import helper

revision = '01c574702284'
down_revision = '533bdd6f24ed'

# List modified/added tables
NEW_TABLES = []
UPDATED_TABLES = ['meetingroom']


# Function which updates schema (create/update tables)


def update_schema(op):
    op.drop_constraint('meetingroom_name_key', 'meetingroom')


def update_data(op):
    if helper.is_mds():
        pass
    else:
        pass


def upgrade():
    if helper.is_mds():
        with helper.schema_update_on_mds(op, NEW_TABLES, UPDATED_TABLES):
            update_schema(op)
        pass
    else:

        with helper.schema_update_on_main(op, NEW_TABLES):
            update_schema(op)
        update_data(op)


def downgrade():
    pass

"""add_queuefeatures_recording

Revision ID: abbd6671133
Revises: 3483ea9a5aa0

"""

# revision identifiers, used by Alembic.
revision = 'abbd6671133'
down_revision = '3483ea9a5aa0'

from alembic import op
from sqlalchemy.schema import Column
from sqlalchemy.types import Integer, Enum

recording_type = Enum('notrecorded', 'recorded', 'recordedondemand', name='queuefeatures_recordingmode')


def upgrade():
    recording_type.create(op.get_bind())
    op.add_column('queuefeatures',
                  Column('recording_mode',
                         recording_type,
                         nullable=False,
                         server_default='notrecorded'))
    op.add_column('queuefeatures',
                  Column('recording_activated',
                         Integer,
                         nullable=False,
                         server_default='0'))


def downgrade():
    op.drop_column('queuefeatures', 'recording_mode')
    op.drop_column('queuefeatures', 'recording_activated')
    op.execute("DROP TYPE IF EXISTS queuefeatures_recordingmode")

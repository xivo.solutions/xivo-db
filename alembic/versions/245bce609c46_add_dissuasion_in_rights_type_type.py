"""add-dissuasion-in-rights_type-type

Revision ID: 245bce609c46
Revises: d17f9d644582

"""

# revision identifiers, used by Alembic.
revision = '245bce609c46'
down_revision = 'd17f9d644582'

from alembic import op


def upgrade():
    connection = None
    if not op.get_context().as_sql:
        connection = op.get_bind()
        connection.execution_options(isolation_level='AUTOCOMMIT')

    op.execute("ALTER TYPE right_type ADD VALUE IF NOT EXISTS 'dissuasion_access'")


def downgrade():
    pass

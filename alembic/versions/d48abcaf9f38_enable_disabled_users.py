"""Enable disabled users

Revision ID: d48abcaf9f38
Revises: 5070c2415d6f

"""

# revision identifiers, used by Alembic.
revision = 'd48abcaf9f38'
down_revision = '5070c2415d6f'

import sqlalchemy as sa
from alembic import op

from xivo_db import helper

userfeatures_table = sa.sql.table('userfeatures',
                                  sa.sql.column('id'),
                                  sa.sql.column('firstname'),
                                  sa.sql.column('lastname'),
                                  sa.sql.column('commented'))


def _get_disabled_users():
    query = (sa.sql.select([userfeatures_table.c.id,
                            userfeatures_table.c.firstname,
                            userfeatures_table.c.lastname,
                            userfeatures_table.c.id])
             .where(userfeatures_table.c.commented == 1))

    return op.get_bind().execute(query)


def _enable_disabled_users():
    for user in _get_disabled_users():
        fullname = u" ".join([_f for _f in [user.firstname, user.lastname] if _f])
        print(u'[MIGRATE_DISABLED_USERS] - INFO: Enabling user "{}" ({})'.format(user.id, fullname))

        query = (userfeatures_table.update()
                 .values(commented=0)
                 .where(userfeatures_table.c.id == user.id))

        op.get_bind().execute(query)


def upgrade():
    if helper.is_mds():
        pass
    else:
        _enable_disabled_users()


def downgrade():
    pass

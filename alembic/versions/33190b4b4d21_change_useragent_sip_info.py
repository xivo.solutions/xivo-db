"""change useragent sip info

Revision ID: 33190b4b4d21
Revises: 510c9ca41edf

"""

# revision identifiers, used by Alembic.
revision = '33190b4b4d21'
down_revision = '510c9ca41edf'

import sqlalchemy as sa
from alembic import op
from sqlalchemy import sql

staticsip_table = sa.sql.table('staticsip',
                               sql.column('var_name'),
                               sql.column('var_val'))


def upgrade():
    op.execute(staticsip_table.update().
               where(sa.sql.and_(
        staticsip_table.c.var_name == u'useragent',
        staticsip_table.c.var_val == u'XiVO PBX')).
               values(var_val='XIVO_VERSION'))


def downgrade():
    op.execute(staticsip_table.update().
               where(sa.sql.and_(
        staticsip_table.c.var_name == u'useragent',
        staticsip_table.c.var_val == u'XIVO_VERSION')).
               values(var_val='XiVO PBX'))

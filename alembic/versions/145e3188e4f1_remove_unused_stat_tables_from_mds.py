"""remove unused stat tables from mds

Revision ID: 145e3188e4f1
Revises: 759e04ad2ef9

"""

# revision identifiers, used by Alembic.
revision = '145e3188e4f1'
down_revision = '759e04ad2ef9'

from alembic import op

from xivo_db import helper


def check_publication_exists(publication):
    sql = "SELECT EXISTS (SELECT 1 " \
          "FROM pg_publication " \
          "WHERE pubname = '" + publication + "')"

    result_proxy = op.get_bind().execute(sql)
    exists = result_proxy.fetchone()[0]

    return exists


def check_table_publicated(table):
    sql = "SELECT EXISTS (SELECT 1 " \
          "FROM pg_publication_tables " \
          "WHERE tablename = '" + table + "')"

    result_proxy = op.get_bind().execute(sql)
    exists = result_proxy.fetchone()[0]

    return exists


TABLES = ['stat_agent_periodic',
          'stat_call_on_queue',
          'stat_switchboard_queue']


def upgrade():
    if helper.is_mds():
        for table in TABLES:
            op.execute("GRANT INSERT, UPDATE, DELETE, TRUNCATE, REFERENCES ON TABLE " + table + " TO asterisk")
    else:
        if check_publication_exists('mds'):
            for table in TABLES:
                if check_table_publicated(table):
                    op.execute("ALTER PUBLICATION mds DROP TABLE " + table)


def downgrade():
    pass

"""add meetingroom view and trigram extension

Revision ID: addb1d5f2e27
Revises: 01c574702284

"""

# revision identifiers, used by Alembic.
revision = 'addb1d5f2e27'
down_revision = '01c574702284'

from alembic import op

from xivo_db import helper, postgres


def add_search_extensions():
    new_extensions = ['pg_trgm', 'fuzzystrmatch']
    conn = postgres.create_db_connection('asterisk')
    postgres.create_extensions(conn, new_extensions)


# Function which add a view
def update_schema(op):
    op.execute("""
        CREATE INDEX meetingroom_idx_display_name_gin ON meetingroom USING GIN (to_tsvector('simple', display_name));
    """)


def upgrade():
    if helper.is_mds():
        # We do not add this view on the MDS
        pass
    else:
        # The meetingroom_search will be added on the Main only
        update_schema(op)
        add_search_extensions()


def downgrade():
    pass

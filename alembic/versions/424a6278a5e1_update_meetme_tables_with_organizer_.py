"""Update meetme tables with organizer informations

Revision ID: 424a6278a5e1
Revises: 29331e1f8340

"""

# revision identifiers, used by Alembic.
revision = '424a6278a5e1'
down_revision = '29331e1f8340'

from alembic import op
from sqlalchemy import Enum
from sqlalchemy.sql.schema import Column
from sqlalchemy.types import INTEGER, VARCHAR, TEXT


def upgrade():
    op.drop_table('meetmeguest')
    op.alter_column('meetmefeatures', 'name', type_=VARCHAR(128))
    op.alter_column('meetmefeatures',
                    'user_musiconhold',
                    new_column_name='musiconhold')
    op.add_column('meetmefeatures', Column('user_pin', VARCHAR(12)))
    op.add_column('meetmefeatures', Column('admin_pin', VARCHAR(12)))
    op.add_column('meetmefeatures',
                  Column('user_waitingroom',
                         INTEGER,
                         nullable=False,
                         server_default='0'))
    op.alter_column('meetmefeatures',
                    'user_quiet',
                    new_column_name='quiet')
    op.alter_column('meetmefeatures',
                    'user_announceusercount',
                    new_column_name='announceusercount')

    op.drop_column('meetmefeatures', 'admin_typefrom')
    op.drop_column('meetmefeatures', 'admin_internalid')
    op.drop_column('meetmefeatures', 'admin_externalid')
    op.drop_column('meetmefeatures', 'admin_identification')
    op.drop_column('meetmefeatures', 'admin_mode')
    op.drop_column('meetmefeatures', 'admin_announceusercount')
    op.drop_column('meetmefeatures', 'admin_announcejoinleave')
    op.drop_column('meetmefeatures', 'admin_moderationmode')
    op.drop_column('meetmefeatures', 'admin_initiallymuted')
    op.drop_column('meetmefeatures', 'admin_musiconhold')
    op.drop_column('meetmefeatures', 'admin_poundexit')
    op.drop_column('meetmefeatures', 'admin_quiet')
    op.drop_column('meetmefeatures', 'admin_starmenu')
    op.drop_column('meetmefeatures', 'admin_closeconflastmarkedexit')
    op.drop_column('meetmefeatures', 'admin_enableexitcontext')
    op.drop_column('meetmefeatures', 'admin_exitcontext')
    op.drop_column('meetmefeatures', 'user_mode')
    op.drop_column('meetmefeatures', 'user_initiallymuted')
    op.drop_column('meetmefeatures', 'user_poundexit')
    op.drop_column('meetmefeatures', 'user_starmenu')
    op.drop_column('meetmefeatures', 'user_enableexitcontext')
    op.drop_column('meetmefeatures', 'user_exitcontext')
    op.drop_column('meetmefeatures', 'talkeroptimization')
    op.drop_column('meetmefeatures', 'talkerdetection')
    op.drop_column('meetmefeatures', 'durationm')
    op.drop_column('meetmefeatures', 'closeconfdurationexceeded')
    op.drop_column('meetmefeatures', 'nbuserstartdeductduration')
    op.drop_column('meetmefeatures', 'timeannounceclose')
    op.drop_column('meetmefeatures', 'startdate')
    op.drop_column('meetmefeatures', 'emailfrom')
    op.drop_column('meetmefeatures', 'emailfromname')
    op.drop_column('meetmefeatures', 'emailsubject')
    op.drop_column('meetmefeatures', 'emailbody')
    op.drop_column('meetmefeatures', 'user_hiddencalls')

    op.execute('DROP TYPE meetmefeatures_admin_typefrom')
    op.execute('DROP TYPE meetmefeatures_admin_identification')
    op.execute('DROP TYPE meetmefeatures_mode')

    op.execute("""WITH meetmeconf AS (
    SELECT
           c.room_array[1] as confno,
           c.room_array[2] as user_pin,
           c.room_array[3] as admin_pin
     FROM (
          SELECT string_to_array(var_val, ',') as room_array
          FROM staticmeetme s
          WHERE s.category='rooms' and var_name='conf'
     ) c
    )

    UPDATE meetmefeatures m SET user_pin=c.user_pin, admin_pin=c.admin_pin
    FROM meetmeconf c WHERE m.confno=c.confno""")

    op.execute("ALTER TABLE meetmefeatures DROP CONSTRAINT IF EXISTS meetmefeatures_name_key")


def downgrade():
    meetmefeatures_admin_typefrom = Enum('none', 'internal',
                                         'external', 'undefined',
                                         name='meetmefeatures_admin_typefrom')
    meetmefeatures_admin_identification = Enum('callerid', 'pin', 'all',
                                               name='meetmefeatures_admin_identification')
    meetmefeatures_mode = Enum('listen', 'talk', 'all',
                               name='meetmefeatures_mode')
    meetmefeatures_announcejoinleave = Enum('no', 'yes', 'noreview',
                                            name='meetmefeatures_announcejoinleave')
    meetmefeatures_admin_typefrom.create(op.get_bind())
    meetmefeatures_admin_identification.create(op.get_bind())
    meetmefeatures_mode.create(op.get_bind())

    op.add_column('meetmefeatures',
                  Column('user_hiddencalls',
                         INTEGER, nullable=False, server_default='0'))
    op.add_column('meetmefeatures',
                  Column('admin_typefrom',
                         meetmefeatures_admin_typefrom))
    op.add_column('meetmefeatures',
                  Column('admin_internalid',
                         INTEGER))
    op.add_column('meetmefeatures',
                  Column('admin_externalid',
                         VARCHAR(40)))
    op.add_column('meetmefeatures',
                  Column('admin_identification',
                         meetmefeatures_admin_identification,
                         nullable=False,
                         server_default='pin'))
    op.add_column('meetmefeatures',
                  Column('admin_mode',
                         Enum('listen', 'talk', 'all',
                              name='meetmefeatures_mode'),
                         nullable=False,
                         server_default='all'))
    op.add_column('meetmefeatures',
                  Column('admin_announceusercount',
                         INTEGER, nullable=False, server_default='0'))
    op.add_column('meetmefeatures',
                  Column('admin_announcejoinleave',
                         meetmefeatures_announcejoinleave,
                         nullable=False,
                         server_default='no'))
    op.add_column('meetmefeatures',
                  Column('admin_moderationmode',
                         INTEGER, nullable=False, server_default='0'))
    op.add_column('meetmefeatures',
                  Column('admin_initiallymuted',
                         INTEGER, nullable=False, server_default='0'))
    op.add_column('meetmefeatures',
                  Column('admin_musiconhold',
                         VARCHAR(128)))
    op.add_column('meetmefeatures',
                  Column('admin_poundexit',
                         INTEGER, nullable=False, server_default='0'))
    op.add_column('meetmefeatures',
                  Column('admin_quiet',
                         INTEGER, nullable=False, server_default='0'))
    op.add_column('meetmefeatures',
                  Column('admin_starmenu',
                         INTEGER, nullable=False, server_default='0'))
    op.add_column('meetmefeatures',
                  Column('admin_closeconflastmarkedexit',
                         INTEGER, nullable=False, server_default='0'))
    op.add_column('meetmefeatures',
                  Column('admin_enableexitcontext',
                         INTEGER, nullable=False, server_default='0'))
    op.add_column('meetmefeatures',
                  Column('admin_exitcontext',
                         VARCHAR(39)))
    op.add_column('meetmefeatures',
                  Column('user_mode',
                         meetmefeatures_mode,
                         nullable=False,
                         server_default='all'))
    op.add_column('meetmefeatures',
                  Column('user_initiallymuted',
                         INTEGER, nullable=False, server_default='0'))
    op.add_column('meetmefeatures',
                  Column('user_poundexit',
                         INTEGER, nullable=False, server_default='0'))
    op.add_column('meetmefeatures',
                  Column('user_starmenu',
                         INTEGER, nullable=False, server_default='0'))
    op.add_column('meetmefeatures',
                  Column('user_enableexitcontext',
                         INTEGER, nullable=False, server_default='0'))
    op.add_column('meetmefeatures',
                  Column('user_exitcontext',
                         VARCHAR(39)))
    op.add_column('meetmefeatures',
                  Column('talkeroptimization',
                         INTEGER, nullable=False, server_default='0'))
    op.add_column('meetmefeatures',
                  Column('talkerdetection',
                         INTEGER, nullable=False, server_default='0'))
    op.add_column('meetmefeatures',
                  Column('durationm',
                         INTEGER))
    op.add_column('meetmefeatures',
                  Column('closeconfdurationexceeded',
                         INTEGER, nullable=False, server_default='0'))
    op.add_column('meetmefeatures',
                  Column('nbuserstartdeductduration',
                         INTEGER))
    op.add_column('meetmefeatures', Column('timeannounceclose', INTEGER))
    op.add_column('meetmefeatures', Column('startdate', INTEGER))
    op.add_column('meetmefeatures', Column('emailfrom', VARCHAR(255)))
    op.add_column('meetmefeatures', Column('emailfromname', VARCHAR(255)))
    op.add_column('meetmefeatures', Column('emailsubject', VARCHAR(255)))
    op.add_column('meetmefeatures', Column('emailbody', TEXT,
                                           nullable=False, server_default=''))

    op.alter_column('meetmefeatures',
                    'announceusercount',
                    new_column_name='user_announceusercount')
    op.alter_column('meetmefeatures',
                    'quiet',
                    new_column_name='user_quiet')
    op.drop_column('meetmefeatures', 'user_waitingroom')
    op.drop_column('meetmefeatures', 'admin_pin')
    op.drop_column('meetmefeatures', 'user_pin')
    op.alter_column('meetmefeatures',
                    'musiconhold',
                    new_column_name='user_musiconhold')
    op.alter_column('meetmefeatures', 'name', type_=VARCHAR(80))
    op.create_table(
        'meetmeguest',
        Column('id', INTEGER, primary_key=True, autoincrement=True),
        Column('meetmefeaturesid', INTEGER, nullable=False),
        Column('fullname', VARCHAR(255), nullable=False),
        Column('telephonenumber', VARCHAR(40)),
        Column('email', VARCHAR(320))
    )

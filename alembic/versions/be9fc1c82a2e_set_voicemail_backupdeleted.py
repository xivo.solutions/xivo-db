"""Set voicemail backupdeleted

Revision ID: be9fc1c82a2e
Revises: f5ed9ff8bd89

"""

# revision identifiers, used by Alembic.
revision = 'be9fc1c82a2e'
down_revision = 'f5ed9ff8bd89'

from alembic import op
from sqlalchemy import sql

from xivo_db import helper

staticvoicemail_table = sql.table('staticvoicemail',
                                  sql.column('id'),
                                  sql.column('var_name'),
                                  sql.column('var_val'))


def update_backupdeleted():
    select_query = (sql.select([staticvoicemail_table.c.var_val])
                    .where(staticvoicemail_table.c.var_name == 'backupdeleted'))

    current_value = op.get_bind().execute(select_query).scalar()
    new_value = '10'

    update_query = (staticvoicemail_table
                    .update()
                    .values(
        var_val=new_value)
                    .where(staticvoicemail_table.c.var_name == 'backupdeleted'))

    if current_value != new_value:
        print('[MIGRATE_BACKUPDELETED] : Updating max number of deleted voicemail messages from %s to %s' % (
        current_value, new_value))
        op.get_bind().execute(update_query)


def upgrade():
    if helper.is_mds():
        pass
    else:
        update_backupdeleted()


def downgrade():
    pass

"""Add value ivr to dialaction_action enum

Revision ID: a49c8eadf58f
Revises: 38c4a79d9abe

"""

# revision identifiers, used by Alembic.
revision = 'a49c8eadf58f'
down_revision = '38c4a79d9abe'

from alembic import op

from xivo_db import helper

# List modified/added tables
NEW_TABLES = []
UPDATED_TABLES = []


# Function which updates schema (create/update tables)
def update_schema(op):
    connection = None
    if not op.get_context().as_sql:
        connection = op.get_bind()
        connection.execution_options(isolation_level='AUTOCOMMIT')

    op.execute("ALTER TYPE dialaction_action ADD VALUE 'ivr' AFTER 'meetingroom'")


def upgrade():
    if helper.is_mds():
        with helper.schema_update_on_mds(op, NEW_TABLES, UPDATED_TABLES):
            update_schema(op)
    else:
        with helper.schema_update_on_main(op, NEW_TABLES):
            update_schema(op)


def downgrade():
    pass

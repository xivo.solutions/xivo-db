"""3970 Generate random xuc password

Revision ID: ea47634ce5c3
Revises: 184a72a2c4c7

"""

# revision identifiers, used by Alembic.

revision = 'ea47634ce5c3'
down_revision = '184a72a2c4c7'

import random
import string

import sqlalchemy as sa
from alembic import op

from xivo_db import helper


def generate_password(length=32):
    pool = string.digits + string.ascii_letters + string.punctuation
    return ''.join(random.choice(pool) for _ in range(length))


def upgrade():
    # Update data only on main
    if not helper.is_mds():
        user_table = sa.sql.table('userfeatures',
                                  sa.sql.column('loginclient'),
                                  sa.sql.column('passwdclient'))

        op.execute(
            user_table
                .update()
                .where(user_table.c.loginclient == 'xuc')
                .values(passwdclient=generate_password()))


def downgrade():
    pass

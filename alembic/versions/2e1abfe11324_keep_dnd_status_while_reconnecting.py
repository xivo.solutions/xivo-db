"""Keep DND status while reconnecting

Revision ID: 2e1abfe11324
Revises: 145e3188e4f1

"""

# revision identifiers, used by Alembic.
revision = '2e1abfe11324'
down_revision = '145e3188e4f1'

import re

import sqlalchemy as sa
from alembic import op

from xivo_db import helper

ctistatus_table = sa.sql.table('ctistatus',
                               sa.sql.column('id'),
                               sa.sql.column('name'),
                               sa.sql.column('actions'))


def _correctAction(currentAction):
    pattern = ',enablednd\(false\)|enablednd\(false\),*'
    outputAction = currentAction
    removedCharacters = 0

    for match in re.finditer(pattern, currentAction):
        startMatching = match.start()
        endMatching = match.end()
        outputAction = outputAction[:startMatching - removedCharacters] + outputAction[endMatching - removedCharacters:]
        removedCharacters += endMatching - startMatching

    return outputAction


def _updateRow(correctedAction, row_id):
    op.execute(ctistatus_table
               .update()
               .values(
        actions=correctedAction)
               .where(ctistatus_table.c.id == row_id))


def upgrade():
    if helper.is_mds():
        pass
    else:
        conn = op.get_bind()
        rows = conn.execute(sa.sql.select([ctistatus_table]).
                            where(ctistatus_table.c.name == 'available'))
        for row in rows:
            correctedAction = _correctAction(row[2])
            _updateRow(correctedAction, row[0])


def downgrade():
    pass

"""Clean outcall from contextmember table

Revision ID: 1945863164ca
Revises: c4fab49306c0

"""

# revision identifiers, used by Alembic.
revision = '1945863164ca'
down_revision = 'c4fab49306c0'

import sqlalchemy as sa
from alembic import op

from xivo_db import helper

contextmember_table = sa.sql.table('contextmember',
                                   sa.sql.column('type')
                                   )


def upgrade():
    if helper.is_mds():
        # Delete nothing on MDS as data are replicated
        pass
    else:
        delete_outcall_contextmember = (contextmember_table
                                        .delete()
                                        .where(contextmember_table.c.type == 'outcall'))
        op.get_bind().execute(delete_outcall_contextmember)


def downgrade():
    pass

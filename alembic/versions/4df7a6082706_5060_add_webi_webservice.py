"""5060_add_webi_webservice

Revision ID: 4df7a6082706
Revises: b9a928f027e6

"""

# revision identifiers, used by Alembic.
revision = '4df7a6082706'
down_revision = 'b9a928f027e6'

from alembic import op
import sqlalchemy as sa
from xivo_db import helper


# List modified/added tables
NEW_TABLES=[]
UPDATED_TABLES=[]

# Function which updates schema (create/update tables)
def update_schema(op):
    # Create/Update table
    # Add types ...
    # Add foreing keys
    pass

def update_data(op):
    if helper.is_mds():
        pass
    else:
        op.execute("""
            INSERT INTO "accesswebservice" (name, host, acl) VALUES ('xivo-webi', '172.18.1.250', '{}');
        """)
        op.execute("""
            UPDATE "provisioning" SET net4_ip_rest='0.0.0.0' WHERE net4_ip_rest='127.0.0.1';
        """)

def upgrade():
    # Note: If you create a table using sqlalchemy command:
    #  - command will be run as asterisk,
    #  - therefore the owner's object will be asterisk
    #  - and therefore postgresql will give stats user the SELECT privilege

    if helper.is_mds():
        # Add here the actions applicable to MDS.
        # Don't forget that data are replicated from Main to MDS.

        # To update schema on MDS (see also the README.md) use this:
        with helper.schema_update_on_mds(op, NEW_TABLES, UPDATED_TABLES):
            update_schema(op)
        # Do not update **data** on MDS
        pass
    else:
        # Add here the actions applicable to XiVO Main
        # Don't forget that data are replicated from Main to MDS.

        # To update schema on Main (see also the README.md) use this:
        with helper.schema_update_on_main(op, NEW_TABLES):
            update_schema(op)
        # Update data on Main (and on main only, at least for all replicated tables)
        update_data(op)


def downgrade():
    pass

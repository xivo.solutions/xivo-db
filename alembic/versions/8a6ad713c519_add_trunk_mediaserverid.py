"""Add trunk mediaserverid

Revision ID: 8a6ad713c519
Revises: 335f0d173f38

"""

# revision identifiers, used by Alembic.
revision = '8a6ad713c519'
down_revision = '335f0d173f38'

import sqlalchemy as sa
from alembic import op


def upgrade():
    op.add_column('trunkfeatures',
                  sa.Column('mediaserverid', sa.Integer, nullable=True))
    op.create_foreign_key('trunkfeatures_mediaserverid_fkey', 'trunkfeatures',
                          'mediaserver', ['mediaserverid'], ['id'],
                          ondelete='RESTRICT')


def downgrade():
    op.drop_column('trunkfeatures', 'mediaserverid')

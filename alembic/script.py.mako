"""${message}

Revision ID: ${up_revision}
Revises: ${down_revision}

"""

# revision identifiers, used by Alembic.
revision = ${repr(up_revision)}
down_revision = ${repr(down_revision)}

from alembic import op
import sqlalchemy as sa
from xivo_db import helper
${imports if imports else ""}

# List modified/added tables
NEW_TABLES=[]
UPDATED_TABLES=[]

# Function which updates schema (create/update tables)
def update_schema(op):
    # Create/Update table
    # Add types ...
    # Add foreing keys
    pass

def update_data(op):
    if helper.is_mds():
        pass
    else:
        # Update data
        pass


def upgrade():
    # Note: If you create a table using sqlalchemy command:
    #  - command will be run as asterisk,
    #  - therefore the owner's object will be asterisk
    #  - and therefore postgresql will give stats user the SELECT privilege

    if helper.is_mds():
        # Add here the actions applicable to MDS.
        # Don't forget that data are replicated from Main to MDS.

        # To update schema on MDS (see also the README.md) use this:
        with helper.schema_update_on_mds(op, NEW_TABLES, UPDATED_TABLES):
            update_schema(op)
        # Do not update **data** on MDS
        pass
    else:
        # Add here the actions applicable to XiVO Main
        # Don't forget that data are replicated from Main to MDS.

        # To update schema on Main (see also the README.md) use this:
        with helper.schema_update_on_main(op, NEW_TABLES):
            update_schema(op)
        # Update data on Main (and on main only, at least for all replicated tables)
        update_data(op)


def downgrade():
    ${downgrades if downgrades else "pass"}

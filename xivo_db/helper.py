# -*- coding: UTF-8 -*-

# Copyright (C) 2019 Avencall
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

import os
import subprocess
from uuid import getnode as get_mac

import xivo_db.path
from xivo_db.exception import DBError

FILE_HEADER = '''
#!/bin/bash
set -e
'''


def is_mds():
    try:
        if os.getenv('IS_MDS') == 'true':
            return True
        else:
            return False
    except:
        return False


def get_mds_name_fallback_to_mac():
    name = os.getenv('MDS_NAME')
    if name is None:
        return '_'.join(("%012X" % get_mac())[i:i + 2] for i in range(0, 12, 2))
    else:
        return name


def get_callisto_publisher_sql():
    return get_sql_from_file(xivo_db.path.PG_CALLISTO_PUBLISH_SQL_PATH)


def get_sql_from_file(filepath):
    sql_text = ''
    with open(filepath, "r") as fd:
        for line in fd:
            sql_text += line.replace("\n", " ")
    sql_cmds = sql_text.split(';')[:-1]

    return sql_cmds


def provision_deferred_call(filename, action):
    with open(os.path.join(xivo_db.path.get_deferred_path(), filename), 'w') as fd:
        fd.write(action)


def create_deferred_action(runnable, args=[]):
    action = " ".join([runnable] + args)
    return action


def process_deferred():
    for root, dirs, files in os.walk(xivo_db.path.get_deferred_path()):
        for single_file in files:
            if '.sh' in single_file:
                run_file(os.path.join(root, single_file))
                remove_file(os.path.join(root, single_file))
    xivo_db.path.clean_deferred_path()


def run_file(filepath):
    call_cmd(['bash'] + [filepath])


def remove_file(filepath):
    os.remove(filepath)


def call_cmd(cmd):
    if subprocess.call(cmd, cwd='/tmp'):
        raise DBError()


def check_publication_exists(op, publication):
    sql = "SELECT EXISTS (SELECT 1 " \
          "FROM pg_publication " \
          "WHERE pubname = '" + publication + "')"

    result_proxy = op.get_bind().execute(sql)
    exists = result_proxy.fetchone()[0]

    return exists


def check_subscription_exists(op, subscription):
    sql = "SELECT EXISTS (SELECT 1 " \
          "FROM pg_subscription " \
          "WHERE subname = '" + subscription + "')"

    result_proxy = op.get_bind().execute(sql)
    exists = result_proxy.fetchone()[0]

    return exists


def setup_subscriber_deferred():
    template_sql_file = xivo_db.path.PG_SUBSCRIBE_TEMPLATE_SQL_PATH
    updated_sql_file = xivo_db.path.PG_SUBSCRIBE_TMP_SQL_PATH
    update_subscription_name(template_sql_file, updated_sql_file)
    provision_deferred_call('setup_subscriber.sh', create_deferred_action(xivo_db.path.PG_RUN_SQL, [updated_sql_file]))


def refresh_publication_deferred():
    template_sql_file = xivo_db.path.PG_REFRESH_PUB_TEMPLATE_SQL_PATH
    updated_sql_file = xivo_db.path.PG_REFRESH_PUB_TMP_SQL_PATH
    update_subscription_name(template_sql_file, updated_sql_file)
    provision_deferred_call('refresh_publication.sh',
                            create_deferred_action(xivo_db.path.PG_RUN_SQL, [updated_sql_file]))


def update_subscription_name(template_sql_file, updated_sql_file):
    copy_cmd = ['cp', template_sql_file, updated_sql_file]
    call_cmd(copy_cmd)

    name = get_mds_name_fallback_to_mac()
    update_cmd = ['sed', '-i', 's/MDS_NAME/' + name + '/g', updated_sql_file]
    call_cmd(update_cmd)


class schema_update_on_mds(object):
    def __init__(self, op, new_tables, updated_tables):
        self.op = op
        self.updated_tables = updated_tables[:]
        self.new_tables = new_tables[:]
        self.subname = "main_" + get_mds_name_fallback_to_mac()

    def __enter__(self):
        if check_subscription_exists(self.op, self.subname):
            # If replication already exists (XiVO >= Callisto)
            # re-grant rights temporarily to asterisk on already existing table on MDS
            for updated_table in self.updated_tables:
                self.op.execute(
                    "GRANT INSERT, UPDATE, DELETE, TRUNCATE, REFERENCES ON TABLE " + updated_table + " TO asterisk")

    def __exit__(self, *args):
        if check_subscription_exists(self.op, self.subname):
            # Revoke rights temporarily granted to asterisk on MDS
            for updated_table in self.updated_tables:
                self.op.execute(
                    "REVOKE INSERT, UPDATE, DELETE, TRUNCATE, REFERENCES ON TABLE " + updated_table + " FROM asterisk")
            # Revoke rights to newly created tables too
            for new_table in self.new_tables:
                self.op.execute(
                    "REVOKE INSERT, UPDATE, DELETE, TRUNCATE, REFERENCES ON TABLE " + new_table + " FROM asterisk")
            # If replication already exists (XiVO >= Callisto),
            # call a deferred action to refresh the publication
            refresh_publication_deferred()


class schema_update_on_main(object):
    def __init__(self, op, new_tables):
        self.op = op
        self.new_tables = new_tables[:]

    def __enter__(self):
        pass

    def __exit__(self, *args):
        if check_publication_exists(self.op, "mds"):
            # If replication already exists (XiVO >= Callisto), add new tables to publication
            for new_table in self.new_tables:
                self.op.execute("ALTER PUBLICATION mds ADD TABLE " + new_table)

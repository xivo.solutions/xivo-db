# -*- coding: utf-8 -*-

# Copyright (C) 2014-2020 Avencall
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

import os.path
import shutil
import tempfile

USR_LIB = '/usr/lib/xivo-db'
VAR_LIB = '/var/lib/xivo-db'
USR_SHARE = '/usr/share/xivo-db'
TMP = '/tmp'

AST_LAST = os.path.join(VAR_LIB, 'update-db', 'asterisk-last')
XIVO_LAST = os.path.join(VAR_LIB, 'update-db', 'xivo-last')

PG_DROP_DB = os.path.join(USR_LIB, 'pg-drop-db')
PG_MERGE_DB = os.path.join(USR_LIB, 'pg-merge-db')
PG_POPULATE_DB = os.path.join(USR_LIB, 'pg-populate-db')
PG_RUN_SQL = os.path.join(USR_LIB, 'pg-run-sql')

PG_CALLISTO_PUBLISH_SQL_PATH = os.path.join(USR_SHARE, 'migrate/setup_publisher_for_callisto.sql')
PG_POPULATE_PUBLISH_SQL_PATH = os.path.join(USR_SHARE, 'populate/setup_publisher.sql')

PG_SUBSCRIBE_TEMPLATE_SQL_PATH = os.path.join(USR_SHARE, 'populate/setup_subscriber.sql')
PG_SUBSCRIBE_TMP_SQL_PATH = os.path.join(TMP, 'setup_subscriber.sql')

PG_REFRESH_PUB_TEMPLATE_SQL_PATH = os.path.join(USR_SHARE, 'migrate/mds_refresh_publication.sql')
PG_REFRESH_PUB_TMP_SQL_PATH = os.path.join(TMP, 'mds_refresh_publication.sql')

XIVO_CHECK_DB_OLD = os.path.join(USR_LIB, 'xivo-check-db-old')
XIVO_UPDATE_DB_OLD = os.path.join(USR_LIB, 'xivo-update-db-old')

__DEFERRED_PATH__ = None


def get_deferred_path():
    global __DEFERRED_PATH__
    if __DEFERRED_PATH__ is None:
        __DEFERRED_PATH__ = tempfile.mkdtemp('xivo-db-deferred')
    return __DEFERRED_PATH__


def clean_deferred_path():
    global __DEFERRED_PATH__
    if __DEFERRED_PATH__ is not None:
        shutil.rmtree(__DEFERRED_PATH__, True)

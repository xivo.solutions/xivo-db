# -*- coding: UTF-8 -*-
#
# Copyright (C) 2014 Avencall
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

import argparse
import logging
import xivo_dao.alchemy.all  # imports all the sqlalchemy model

from xivo_dao.helpers import db_manager
from xivo_dao.helpers.db_manager import Base

from xivo_db import alembic
from xivo_db import helper
from xivo_db import postgres

logger = logging.getLogger(__name__)


def _create_tables():
    logger.info('Creating all tables...')
    Base.metadata.create_all()


def _populate_db():
    logger.info('Populating database...')
    postgres.add_replication_to_asterisk()
    if helper.is_mds():
        postgres.setup_subscriber()
    else:
        postgres.populate_db()
        postgres.setup_publisher()


def _create_user(db_user, db_user_password, schema, xc, sql_extra_query=None):
    if xc:
        schema += ', xc'

    schema += ', ivr'

    conn = postgres.create_db_connection(db_name='asterisk')
    postgres.create_user(conn, db_user, db_user_password)

    # Grant usage on schema xc
    sql_grantusage_schema_xc = 'GRANT USAGE ON SCHEMA xc TO %s' % db_user

    # Grant usage on schema ivr
    sql_grantusage_schema_ivr = 'GRANT USAGE ON SCHEMA ivr TO %s' % db_user

    # Grant rights on already created tables, sequences
    sql_select_right_on_tables = 'GRANT SELECT ON ALL TABLES IN SCHEMA %s TO %s' % (schema, db_user)
    sql_select_right_on_sequences = 'GRANT SELECT ON ALL SEQUENCES IN SCHEMA %s TO %s' % (schema, db_user)
    # Grant default rights for tables, sequences created afterward
    sql_default_privilege_on_tables = 'ALTER DEFAULT PRIVILEGES FOR ROLE asterisk IN SCHEMA %s GRANT SELECT ON TABLES TO %s' % (
    schema, db_user)
    sql_default_privilege_on_sequences = 'ALTER DEFAULT PRIVILEGES FOR ROLE asterisk IN SCHEMA %s GRANT SELECT ON SEQUENCES TO %s' % (
    schema, db_user)
    with conn:
        with conn.cursor() as cursor:
            if xc:
                cursor.execute(sql_grantusage_schema_xc)
            cursor.execute(sql_grantusage_schema_ivr)
            cursor.execute(sql_select_right_on_tables)
            cursor.execute(sql_select_right_on_sequences)
            cursor.execute(sql_default_privilege_on_tables)
            cursor.execute(sql_default_privilege_on_sequences)
            if sql_extra_query is not None:
                cursor.execute(sql_extra_query)


def _create_user_stats(xc=True):
    db_user = 'stats'
    db_user_password = 'stats'

    _create_user(db_user, db_user_password, 'public', xc)


def _create_user_db_replic(xc=True):
    db_user = 'dbreplic'
    db_user_password = 'dbreplic'
    extra_query = 'GRANT SELECT, INSERT, UPDATE ON replication_state TO dbreplic'

    _create_user(db_user, db_user_password, 'public', xc, extra_query)


def _drop_db():
    logger.info('Dropping database...')
    postgres.drop_db()


def _init_db():
    logger.info('Initializing database...')
    postgres.init_db()
    alembic.stamp_head()
    db_manager.init_db_from_config()


def main():
    parsed_args = _parse_args()

    _init_logging(parsed_args.verbose)

    if parsed_args.drop:
        _drop_db()

    if parsed_args.init:
        _init_db()
        _create_tables()
        _populate_db()
        _create_user_stats()
        _create_user_db_replic()


def _parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('-v', '--verbose', action='store_true',
                        help='increase verbosity')
    parser.add_argument('--drop', action='store_true',
                        help='drop database')
    parser.add_argument('--init', action='store_true',
                        help='initialize database')
    return parser.parse_args()


def _init_logging(verbose):
    logger = logging.getLogger()
    handler = logging.StreamHandler()
    handler.setFormatter(logging.Formatter('%(message)s'))
    logger.addHandler(handler)
    if verbose:
        logger.setLevel(logging.INFO)
    else:
        logger.setLevel(logging.ERROR)

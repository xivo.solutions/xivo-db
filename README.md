# xivo-db

[[_TOC_]]

xivo-db:

- creates the postgres-based docker image with the holding db
- and containing the alembic script to upgrade the db

Database can be changed three ways:

1. Changing schema/table specification
2. Populating data in database
3. Adding a migration script

See [private database wiki page](https://gitlab.com/avencall/randd-doc/wikis/coding/database) for a more high level
overview.

## Changing schema/table specification

If you need to change the database schema you must do it twice:

- once for **clean installation**
  - add table in xivo-dao - see xivo.solutions/xivo-dao> README (this will create the table when
    calling `_create_tables()` function in `bin/init_db.py` file)
  - add the table in `populate/setup_publisher.sql` file : this will add it to the replication publication for clean install on XiVO Main (which is done by the call to `postgres.setup_publisher()` in function `_populate_db` in `bin/init_db.py` file)
  - add the table in `populate/setup_subscriber.sql` file : this will revoke the write permission on these tables for the asterisk user on the MDS (which is done by the call to `postgres.setup_subscriber()` in function `_populate_db` in `bin/init_db.py` file))
- once for **migration**
  - create table/update schema through an alembic script (see [Adding a migration script](#adding-a-migration-script))

## Populating data in database

For clean install, edit `populate/populate.sql` file which contains init data.  
For upgrade, consider a migration script below.

## Adding a migration script

### Before adding

:warning:  
  There is a SINGLE tree for db migration. As soon as the latest migration script is done, the following script must start from here. Meaning doing migration script for bugfix/for previous LTS is not possible as soon as their latest migration script isn't the latest one overall. Otherwise an upgrade towards a following version will break the db.

  In other words, do not add migration script for LTS branch if there is at least one done for upcoming LTS.

### Howto

To add a new migration script from your developer machine, go into the root directory of this xivo-db repository. There
should be an `alembic.ini` file in this directory. You can then use the following command to create a new migration
script:

```bash
   PYTHONPATH=. alembic revision -m "<description>"
```

This will create a file `<some hash>_<description>` in the `alembic/versions` directory, which you'll have to edit.  

When the migration scripts are executed, they use a connection to the database with the role/user ``asterisk``. This means that new objects that are created in the migration scripts will be owned by the ``asterisk`` role and it is thus not necessary (nor recommended) to explicitly grant access to objects to the asterisk role (i.e. no "GRANT ALL" command after a "CREATE TABLE" command).  

Note that `down_revision` ID must point to the last merged migration script.  

Scripts should use alembic and/or sqlalchemy though migration scripts can be written in plain SQL commands with the ``.execute`` method.  

## Running unit tests

You can create a python virtualenv if you haven't already

```bash
python3 -m venv db-venv
source db-venv/bin/activate
```

Given you're in a virtualenv :

```bash
pip install -r requirements.txt -r test-requirements.txt
tox
```

## Manually simulating your migration script

You may use this procedure to test your alembic migration script, especially while writing it.  
You may want to do a snapshot beforehand in case you mess up, but you won't have to fallback to it multiple time.
I'll use below:  

- `192.168.56.1` as your XiVO PBX IP address
- `h45h_script_name.py` as the name of the migration script you're writing

Check the postgres logs : `tail -f /var/log/postgresql/*.log`

### 0 Backup your database

Let's save the content of your postgres data folder where you can retrieve it quickly.

```bash
cp -r /var/lib/postgresql/15/ /tmp/psql/
```

### 1 Stop the db container

```bash
xivo-dcomp stop db
xivo-dcomp rm -f db
```

### 2 Restore the backuped database

```bash
rm -rf /var/lib/postgresql/15/data/
cp -r /tmp/psql/data/ /var/lib/postgresql/15/
```

:warning: If you didn't stop the db container as you do that, it'll try to recreate the database with the init-db script. Stop it then restore again.

### 3 Restart the database

```bash
xivo-dcomp up -d
```

You may modify webi/data as you want to change the condition of the migration.

### 4 Charge the alembic script in the db docker

On your host

```bash
scp alembic/versions/h45h_script_name.py root@192.168.56.1:/tmp
```

then on XiVO as root

```bash
docker cp /tmp/h45h_script_name.py xivo-db-1:/usr/share/xivo-db/alembic/versions/
```

### 5 Go inside the container and execute the migration

```bash
docker exec -w /usr/share/xivo-db -it xivo-db-1 bash
```

This leads you within db container where `alembic.ini` is located.
With `alembic current` you can check what is the alembic's verrsion id you're on.
And you shall be able to upgrade it (given the alembic script you charged at step4 lets you go one step further) using `alembic upgrade +1`.

### 6 Try again

Back to [step 1](#1-stop-the-db-container) you can retry the upgrade, maybe with

- an updated version of your migration script (to reload at [step 4](#4-charge-the-alembic-script-in-the-db-docker))
- different data to upgrade (change after [step 3](#3-restart-the-database))

Rather than directly mounting then executing the alembic script, you may use an override file for a specific docker image of xivo-db with your changes if you have one ready, see [below](#testing-database-evolution-in-dev)

## Buil docker image locally

```bash
TARGET_VERSION=test 
./docker_build.sh
```

## Testing Database evolution in dev

During database evolution development, you'll need to test both case :

- The upgrade of an existing database
- The installation of a new database

The easiest way to test this two case is to test these with your XiVO database.

The first step is to build your container with all your modification embedded :

1. First I **strongly advise you** to do a snapshot of the current state of your XiVO. You'll have to restore this
   snapshot after every failed or success test to avoid to stay in inconsistant database state.
2. Then if your change xivo-dao, you need to target your branch in this project
    1. edit the `requirement.txt` and append `@` following with the name of your branch in xivo-dao line.
       Example `git+https://gitlab.com/xivo.solutions/xivo-dao.git@4388-fix-meetingroom-table-unique-constraint`
3. Set TARGET_VERSION variable according in which version you are working (It will be used to tag the image).
   Example : `export TARGET_VERSION=2021.15.00-dev-01`
4. Then build the image locally `./docker_build.sh`
5. Then you need to send your newly built image to your XiVO.
   Example : `docker save xivoxc/xivo-db:2021.15.00-dev-01 | bzip2 | ssh root@XIVO_HOST 'bunzip2 | docker load'`
6. Add or update the file `/etc/docker/xivo/10-db-evol.override.yml` the version of the container to use (:warning: It
   will override the version used by your XiVO don't forget to remove it after if you don't reload the snapshot).
   Example :

```yml
services:
  db:
   image: "xivoxc/xivo-db:2021.15.00-dev-01"
```

### Test upgrade

1. Execute `xivo-dcomp up -d`. It should recreate your db container and apply your migration script.  You can connect to the database to verify everything is OK. The log are available in `/var/log/postgresql/postgresql-15-main.log`

### Test new database installation

At this point, you can test new installation by removing completely your database datas. Be sure to have a snapshot or agree to lose all your postgres datas before executing next commands.

```bash
xivo-dcomp stop db
xivo-dcomp rm db
rm -rf /var/lib/postgresql/15/
xivo-dcomp up -d
```

As above ,you can connect to the database and check the logs.

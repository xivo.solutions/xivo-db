check_database_recording_exist() {
  psql -U postgres -tc "SELECT 1 FROM pg_database WHERE datname = 'recording'" | grep -q 1
}

check_database_spagobi_exist() {
  psql -U postgres -tc "SELECT 1 FROM pg_database WHERE datname = 'spagobi'" | grep -q 1
}

create_database_recording() {
  psql -U postgres <<-EOF
	CREATE USER recording WITH PASSWORD 'recording';

  CREATE DATABASE recording WITH OWNER recording;
	EOF

  echo 'recording, spagobi database creation; Done'
}

create_database_spagobi() {
  psql -U postgres <<-EOF
  CREATE USER spagobi WITH PASSWORD 'spagobi';

  CREATE DATABASE spagobi WITH OWNER spagobi;
	EOF

  echo 'recording, spagobi database creation; Done'
}

main() {
  if [ "${IS_MDS}" == "true" ]; then
    echo 'recording, spagobi installation is not required on MDS'
  else
    if [ "${UC:-"false"}" == "yes" ]; then
      echo
      echo 'Creating recording, spagobi database...'
      if ! check_database_recording_exist; then
        create_database_recording
        echo 'Done; recording database is up and running'
        echo
      else
        echo 'Skipping; recording database Already exist'
        echo
      fi
      if ! check_database_spagobi_exist; then
        create_database_spagobi
        echo 'Done; spagobi database is up and running'
        echo
      else
        echo 'Skipping; spagobi database Already exist'
        echo
      fi
    else
      echo 'recording, spagobi installation is not required on default environment'
    fi
  fi
}

main "${@}"

#!/bin/bash

set -e

create_database() {
  psql -U postgres <<- EOF
	CREATE DATABASE usm;
	REVOKE ALL ON DATABASE usm FROM PUBLIC;
	EOF

  echo 'USM database creation; Done'
}

create_schema() {
  psql -U postgres usm <<- EOF
  -- event schema
	CREATE SCHEMA evt;
	CREATE ROLE read_write_event;
	GRANT CONNECT ON DATABASE usm TO read_write_event;
	GRANT USAGE,CREATE ON SCHEMA evt TO read_write_event;
	GRANT SELECT, INSERT, UPDATE, DELETE ON ALL TABLES IN SCHEMA evt TO read_write_event;
	GRANT USAGE ON ALL SEQUENCES IN SCHEMA evt TO read_write_event;
  -- config schema
	CREATE SCHEMA cfg;
	CREATE ROLE read_write_config;
	GRANT CONNECT ON DATABASE usm TO read_write_config;
	GRANT USAGE,CREATE ON SCHEMA cfg TO read_write_config;
	GRANT SELECT, INSERT, UPDATE, DELETE ON ALL TABLES IN SCHEMA cfg TO read_write_config;
	GRANT USAGE ON ALL SEQUENCES IN SCHEMA cfg TO read_write_config;
  -- collector ro access
	CREATE ROLE read_only;
  GRANT CONNECT ON DATABASE usm TO read_only;
	GRANT USAGE ON SCHEMA evt,cfg TO read_only;
	GRANT SELECT ON ALL TABLES IN SCHEMA evt,cfg TO read_only;
  GRANT SELECT ON ALL SEQUENCES IN SCHEMA evt,cfg TO read_only;
	REVOKE CREATE ON SCHEMA public,evt,cfg FROM read_only,public;
	EOF

  echo 'USM event and config schemas creation; Done'
}

create_users() {
  psql -U postgres usm <<- EOF
  -- event user
	CREATE USER event WITH PASSWORD '${USM_EVENT_SECRET}';
	ALTER DEFAULT PRIVILEGES FOR USER event IN SCHEMA evt GRANT SELECT, INSERT, UPDATE, DELETE ON TABLES TO read_write_event;
	ALTER DEFAULT PRIVILEGES FOR USER event IN SCHEMA evt GRANT USAGE ON SEQUENCES TO read_write_event;
	GRANT read_write_event TO event;
  -- config user
	CREATE USER config WITH PASSWORD '${USM_CONFIG_SECRET}';
	ALTER DEFAULT PRIVILEGES FOR USER config IN SCHEMA cfg GRANT SELECT, INSERT, UPDATE, DELETE ON TABLES TO read_write_config;	
	ALTER DEFAULT PRIVILEGES FOR USER config IN SCHEMA cfg GRANT USAGE ON SEQUENCES TO read_write_config;
	GRANT read_write_config TO config;
  -- collector user
  CREATE USER collector WITH PASSWORD '${USM_COLLECTOR_SECRET}';
  ALTER DEFAULT PRIVILEGES FOR USER event,config IN SCHEMA evt,cfg GRANT SELECT ON TABLES TO read_only;
  ALTER DEFAULT PRIVILEGES FOR USER event,config IN SCHEMA evt,cfg GRANT SELECT ON SEQUENCES TO read_only;
	GRANT read_only TO collector;
	EOF

  echo 'USM users creation; Done'
}

update_users() {
  psql -U postgres usm <<- EOF  
	ALTER USER event WITH PASSWORD '${USM_EVENT_SECRET}';
	ALTER USER config WITH PASSWORD '${USM_CONFIG_SECRET}';
  ALTER USER collector WITH PASSWORD '${USM_COLLECTOR_SECRET}';	
	EOF

  echo 'USM users update; Done'
}

check_db_usm_exists() {
  psql -U postgres -tc "SELECT 1 FROM pg_database WHERE datname = 'usm'" | grep -q 1
}

main() {

  if [ "${IS_MDS}" == "true" ]; then
    echo 'USM installation is not required on MDS'
  else
    echo
    echo 'Check USM database exists...'
    if ! check_db_usm_exists; then
      create_database
      create_schema
      create_users
    else
      update_users
    fi
    echo 'Done; USM database is up and running'
    echo
  fi
}

main "${@}"
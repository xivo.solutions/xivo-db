#!/bin/bash

set -e

# Fix UnicodeEncodeError when printing non-ascii characters from alembic scripts
export PYTHONIOENCODING=utf-8

# Update XiVO DB schema
xivo-update-db

echo "$0 update finished"

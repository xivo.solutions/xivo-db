#!/bin/bash

set -e

create_database() {
  psql -U postgres <<- EOF
	CREATE DATABASE call_quality_stats;
	EOF
  echo 'call_quality_stats database creation; Done'
}

create_table_audio_issues() {
  psql -U postgres call_quality_stats  <<- EOF
  CREATE SCHEMA audio;
	CREATE TABLE audio.audio_issues ( id serial PRIMARY KEY, ts TIMESTAMP WITH TIME ZONE ,
  username VARCHAR (64) NOT NULL, rtt INT NOT NULL, jitterup INT NOT NULL, 
  jitterdown INT NOT NULL, lossup INT NOT NULL, lossdown INT NOT NULL, CONSTRAINT unique_ts_username UNIQUE (ts,username));
	EOF

  echo ' audio.audio_issues table creation; Done'
}

check_db_call_quality_stats_exists() {
  psql -U postgres -tc "SELECT 1 FROM pg_database WHERE datname = 'call_quality_stats'" | grep -q 1
}

create_schema() {
   psql -U postgres call_quality_stats <<- EOF
    CREATE ROLE fluentrole;
    GRANT CONNECT ON DATABASE call_quality_stats TO fluentrole;
    GRANT USAGE,CREATE ON SCHEMA audio TO fluentrole;
    GRANT SELECT, INSERT, UPDATE, DELETE ON ALL TABLES IN SCHEMA audio TO fluentrole;
    GRANT USAGE ON ALL SEQUENCES IN SCHEMA audio TO fluentrole;
    REVOKE CREATE on SCHEMA audio from fluentrole;
    CREATE ROLE grafanarole;
    GRANT CONNECT ON DATABASE call_quality_stats TO grafanarole;
    GRANT USAGE ON SCHEMA audio TO grafanarole;
    GRANT SELECT ON ALL TABLES IN SCHEMA audio TO grafanarole;
    GRANT SELECT ON ALL SEQUENCES IN SCHEMA audio TO grafanarole;
    REVOKE CREATE ON SCHEMA audio FROM grafanarole;
	EOF
  
  echo 'call_quality_stats schema creation; Done'
}

create_users() {
  psql -U postgres call_quality_stats <<- EOF
    CREATE USER rwfluent WITH PASSWORD '${FLUENTD_SECRET}';
    ALTER DEFAULT PRIVILEGES IN SCHEMA audio GRANT SELECT, INSERT, UPDATE, DELETE ON TABLES TO fluentrole;
    ALTER DEFAULT PRIVILEGES IN SCHEMA audio GRANT USAGE ON SEQUENCES TO fluentrole;
    GRANT fluentrole TO rwfluent;
    CREATE USER rografana WITH PASSWORD '${GRAFANA_SECRET}';
    ALTER DEFAULT PRIVILEGES IN SCHEMA audio GRANT SELECT ON TABLES TO grafanarole;
    ALTER DEFAULT PRIVILEGES IN SCHEMA audio GRANT SELECT ON SEQUENCES TO grafanarole;
    GRANT grafanarole TO rografana;
	EOF

  echo 'call_quality_stats users creation; Done'
}

update_users() {
  psql -U postgres call_quality_stats <<- EOF  
	ALTER USER rwfluent WITH PASSWORD '${FLUENTD_SECRET}';
	ALTER USER rografana WITH PASSWORD '${GRAFANA_SECRET}';
	EOF

  echo 'call_quality_stats users update; Done'
}

main() {

  if [ "${IS_MDS}" == "true" ]; then
    echo 'call_quality_stats installation is not required on MDS'
  else
    echo
    echo 'Check call_quality_stats database exists...'
    if ! check_db_call_quality_stats_exists; then
      create_database
      create_table_audio_issues
      create_schema
      create_users
    else
      update_users
    fi
    echo 'Done; call_quality_stats database is up and running'
    echo
  fi
}

main "${@}"

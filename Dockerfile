FROM postgres:15.6-bookworm

LABEL maintainer="XiVO R&D Team rannd@xivo.solutions"

RUN localedef -i fr_FR -c -f UTF-8 -A /usr/share/locale/locale.alias fr_FR.UTF-8
ENV LANG fr_FR.utf8

ENV PGDATA /var/lib/postgresql/15/data

ENV DEBIAN_FRONTEND noninteractive
ENV HOME /root

# Add dependencies
RUN apt-get update \
    && apt-get install -y --no-install-recommends --auto-remove \
    git \
    sudo \
    python3 \
    python3-pip \
    python3-setuptools \
    python3-six \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

# Install xivo-db
COPY bin/xivo-init-db \
    bin/xivo-check-db \
    bin/xivo-update-db \
    /usr/bin/

# These binaries must be in /usr/lib/xivo-db. See xivo_db/path.py
COPY bin/pg-merge-db \
     bin/pg-drop-db \
     bin/pg-populate-db \
     bin/pg-run-sql \
     /usr/lib/xivo-db/

WORKDIR /usr/src/xivo-db

COPY requirements.txt .
RUN python3 -m pip install --no-cache-dir -r requirements.txt --break-system-packages

COPY . .
RUN mkdir -p /usr/share/xivo-db \
    && mv alembic.ini /usr/share/xivo-db/ \
    && mv alembic/ /usr/share/xivo-db/alembic/ \
    && mv populate/ /usr/share/xivo-db/populate/ \
    && mv migrate/ /usr/share/xivo-db/migrate/ \
    && mkdir -p /var/lib/xivo-db-deferred \
    && chown -R postgres:postgres /var/lib/xivo-db-deferred

RUN python3 setup.py install

## POSTGRES IMAGE CUSTOMIZATION
# Create updatedb.d dir (see branch add-prestart-hook-to-update-structure
# in docker-library-postgres-xivo-mirror repository)
RUN mkdir -p /docker-entrypoint-updatedb.d
# replace upstream entrypoint script
RUN rm /usr/local/bin/docker-entrypoint.sh \
    && mv docker/docker-entrypoint.sh /usr/local/bin/
## END of POSTGRES IMAGE CUSTOMIZATION

# Add init script
RUN mv docker/00-xivo-init-db.sh /docker-entrypoint-initdb.d/
# Add update script
RUN mv docker/00-xivo-update-db.sh /docker-entrypoint-updatedb.d/
# Add USM database
RUN mv docker/01-xivo-usm-db.sh /docker-entrypoint-updatedb.d/
# Add UC database
RUN mv docker/02-xivo-uc-db.sh /docker-entrypoint-updatedb.d/
# Add callstats database
RUN mv docker/03-xivo-call-quality-stats-db.sh /docker-entrypoint-updatedb.d/

# Add xivo default database configuration
RUN mkdir -p /etc/postgresql/conf.d
COPY conf.d/* /etc/postgresql/conf.d
COPY docker/pg_hba.conf /etc/postgresql/

WORKDIR /var/tmp

# Add pg_hba.conf file and clean source dir
RUN rm -rf /usr/src/xivo-db

# Update default DB URI to use unix socket
RUN mkdir /etc/xivo-dao
RUN echo "db_uri: postgresql://asterisk:proformatique@:5432/asterisk" > /etc/xivo-dao/config.yml

# Configure environment
ENV PYTHONPATH=/usr/local/lib/python3/dist-packages

# Default shutdown mode to 'fast'
STOPSIGNAL SIGINT

# Version
ARG TARGET_VERSION
LABEL version=${TARGET_VERSION}

EXPOSE 5432
USER root
